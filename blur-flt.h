/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NLBIN_BLUR_FLT_H
#define NLBIN_BLUR_FLT_H


void gaussian_blur_1d_u8(unsigned char *dst, int dst_stride,
                         const unsigned char *src, int len, double sigma);

void gaussian_blur_u8(unsigned char *dst, int dst_stride,
                      const unsigned char *src, int width, int height,
                      int src_stride,
                      double x_sigma, double y_sigma, 
                      int batch_size);

void gaussian_blur_int(int *dst, int dst_stride,
                      const int *src, int width, int height,
                      int src_stride,
                      double x_sigma, double y_sigma, 
                      int batch_size);

#endif
