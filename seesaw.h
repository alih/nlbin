/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NLBIN_SEESAW_H
#define NLBIN_SEESAW_H

/** 
 * Computes the height of x when integers are placed on a seesaw.
 * For height = 3, for example, the seesaw looks like this:
 *
 *   2 <-     -4 -3           2 3         8 9
 *   1 <- ...       -2      1     4     7     10       ...
 *   0 <-              -1 0         5 6          11 12
 */
int seesaw(int x, int height);

int seesaw_speed(int x, int height);

enum { SEESAW_MAX_SEGMENTS = 3 };

typedef struct 
{
    int coef;
    int start;
    int end;
} SeesawSegment;

/**
 * Takes a segment [start:end], applies seesaw() to it (height = len)
 * and represents the result as up to 3 segments in [0:len].
 *
 * The [start:end] segment on this seesaw might look thus:
 *
 *                      x x 
 *                x   x     x
 *                  x         x
 * 
 * Same, after projection to [0:len]:
 *
 *   2: xx
 *   1: xxx
 *   0: xx
 *
 * In general, the result will consist of up to 3 segments with different
 * thickness.
 *
 * \returns  The number of segments, maximum 3.
 * \param[out] out_coef     Coefficient (thickness) of a segment.
 * \param[out] out_
 */
int seesaw_segment(SeesawSegment result[SEESAW_MAX_SEGMENTS],
                   int len, int start, int end);

enum { SEESAW_MAX_UPDATES = 3 };

typedef struct
{
    int cap;
    int left;
    int right;
    unsigned char left_backwards;
    unsigned char right_backwards;
} SeesawUpdate;

/**
 * For a sliding window that starts from [left:right],
 * get a sequence of actions needed to move the window 
 * all the way through the [0:n] image.
 */
int seesaw_schedule_updates(
    SeesawUpdate result[SEESAW_MAX_UPDATES],
    int n,
    int left,
    int right);

#endif
