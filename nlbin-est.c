/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "blur-flt.h"
#include "threshold.h"
#include "dilate.h"
#include "nlbin-est.h"
#include "max.h"

void nlbin_estimater_params_set_default(NlbinEstimaterParams *params)
{
    params->escale = 1.0;
    params->border = 0.1;
    params->low = 5;
    params->high = 90;
    params->threshold = 0.5;
    params->batch_size = 32;
}

static int percentile_by_c_hist(const int *c_hist, double percent)
{
    int t = c_hist[255] * percent / 100.0;
    int l = 0;
    int r = 256;
    while (r - l > 1)
    {
        int m = (l + r) / 2;
        if (c_hist[m] < t)
            l = m;
        else
            r = m;
    }
    return l;
}

static int find_threshold_by_histogram(const int *histogram, NlbinEstimaterParams *params)
{
    int c_hist[256];
    int s = 0;
    for (int i = 0; i < 256; i++)
        c_hist[i] = s += histogram[i];

    int lo = percentile_by_c_hist(c_hist, params->low);
    int hi = percentile_by_c_hist(c_hist, params->high);

    return (int) (lo + (hi - lo) * params->threshold);
}

int nlbin_estimate_threshold(const unsigned char *flat,
                             int flat_stride,
                             int width, int height,
                             NlbinEstimaterParams *params)
{
    // This corresponds to the large chunk of python code
    // starting from cutting the border:
    //
    //    d0, d1 = flat.shape
    //    o0, o1 = int(border*d0), int(border*d1)
    //    est = flat[o0:d0-o0, o1:d1-o1]
    //
    // and ending right before the actual thresholding:
    //
    //    bin = np.array(255*(flat > threshold), 'B')

    // first blur
    const double sigma = params->escale * 20.0;

    const int x_border = (int) (width  * params->border);
    const int y_border = (int) (height * params->border);

    const int new_width  = width  - 2 * x_border;
    const int new_height = height - 2 * y_border;

    const int blurred_est_stride = (new_width + 7) & ~7;
    unsigned char *blurred_est = (unsigned char *) 
        malloc(new_height * blurred_est_stride);

    const unsigned char *flat_cropped = flat + y_border * flat_stride 
                                             + x_border;

    gaussian_blur_u8(blurred_est, blurred_est_stride, flat_cropped,
                     new_width, new_height, flat_stride,
                     sigma, sigma, params->batch_size);  

    // squaring
    // (note that we could potentially use u16, 
    //  but we can't due to the way the box filter works).
    
    const int orig_v_stride = blurred_est_stride;
    int *orig_v = (int *) 
        malloc(new_height * orig_v_stride * sizeof(int));
    for (int y = 0; y < new_height; y++)
    {
        const unsigned char *src_row = flat_cropped + y * flat_stride;
        int *dst_row = orig_v + y * orig_v_stride;
        for (int x = 0; x < new_width; x++)
        {
            int d = src_row[x] - blurred_est[y * blurred_est_stride + x];
            dst_row[x] = d * d;
        }
    }
    free(blurred_est);

    const int blurred_v_stride = orig_v_stride;
    int *blurred_v = (int *)
        malloc(new_height * blurred_v_stride * sizeof(int));
    gaussian_blur_int(blurred_v, blurred_v_stride,
                      orig_v, new_width, new_height,
                      orig_v_stride,
                      sigma, sigma, params->batch_size);
    free(orig_v);

    // the original has this:
    //    v = sqrt(v)
    //    v = v > 0.3 * max(v)
    // which is equivalent to:
    //    v = sqrt(v) > 0.3 * max(sqrt(v))
    // max and sqrt commute:
    //    v = sqrt(v) > 0.3 * sqrt(max(v))
    // since 0.3 == sqrt(0.09):
    //    v = sqrt(v) > sqrt(0.09 * max(v))
    // which is simply
    //    v = v > 0.09 * max(v)
    int vmax = max_2d_int(blurred_v, blurred_v_stride, new_width, new_height);

    // pre-dilation mask
    int v_mask_w_in_bytes = (new_width + 7) >> 3;
    int v_mask_stride = (v_mask_w_in_bytes + 3) & ~3;
    unsigned char *v_mask = (unsigned char *) malloc(new_height * v_mask_stride);
    threshold_int(v_mask, v_mask_stride, blurred_v, blurred_v_stride,
                  new_width, new_height, (int) 0.09 * vmax);
    free(blurred_v);
    
    int histogram[256];
    memset(histogram, 0, sizeof(histogram));
    int dilation = (int) (params->escale * 50);
    dilate_2d_histogram(histogram, flat_cropped, flat_stride,
                        v_mask, v_mask_stride,
                        new_width, new_height,
                        dilation, dilation);
    free(v_mask);

    return find_threshold_by_histogram(histogram, params);
}
