/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is a port of binarizer from ocropus and kraken from Python to C.
// The originals (ocropus and kraken, resp.) are at
//     https://github.com/tmbdev/ocropy/blob/master/ocropus-nlbin
//     https://github.com/mittagessen/kraken/blob/master/kraken/binarization.py

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <g4-easyapi.h>
#include "rank-flt.h"
#include "nlbin-est.h"
#include "threshold.h"
#include "pgm.h"
#include "jpeg.h"

typedef struct
{
    int perc;
    int range;
    const char *flat_image_path; // unowned
    NlbinEstimaterParams est_params;
} NlbinBinarizerParams;

static void nlbin_params_set_default(NlbinBinarizerParams *params)
{
    params->perc = 80;
    params->range = 40; // python has 20 but it downscales x2
    params->flat_image_path = NULL;
    nlbin_estimater_params_set_default(&params->est_params);
}

static void print_usage(const char *prog_name)
{
    NlbinBinarizerParams params;
    nlbin_params_set_default(&params);
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s [options] input.(jpg|pgm) output.tif\n", prog_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    --flat output.pgm    Save flattened image\n");
    fprintf(stderr, "    --perc value         Flattening rank filter percentile (default: %d)\n",
                     params.perc);
    fprintf(stderr, "    --range value        Length of flattening rank filter (default: %d)\n",
                     params.range);
    fprintf(stderr, "                         (note: equals range from python code divided by zoom)\n");
    fprintf(stderr, "    --escale value       Scale of threshold estimator's distances (default: %.2lf)\n",
                     params.est_params.escale);
    fprintf(stderr, "    --border value       Part of the image to crop as a border (default: %.2lf)\n",
                     params.est_params.border);
    fprintf(stderr, "    --low value          Percentile for the low intensity value (default: %.2lf)\n",
                     params.est_params.low);
    fprintf(stderr, "    --high value         Percentile for the high intensity value (default: %.2lf)\n",
                     params.est_params.high);
    fprintf(stderr, "    --threshold value    Position of the threshold between low and high (default: %.2lf)\n",
                     params.est_params.threshold);
    fprintf(stderr, "    --batch-size         (a low-level parameter of 2d filters) (default: %d)\n",
                     params.est_params.batch_size);
}

static void nlbin(const char *input_path,
                  const char *output_path,
                  NlbinBinarizerParams *params)
{
    FILE *in = stdin;
    if (strcmp(input_path, "-"))
        in = fopen(input_path, "rb");

    int width;
    int height; 
    int stride;

    unsigned char *image;
    int c = getc(in);
    ungetc(c, in);
    if (c == 'P')
    {
        image = pgm_read(in, &width, &height);
        stride = width;
    }
    else
    {
        image = jpeg_read_gray(in, &width, &height, &stride);
    }
    if (!image)
    {
        perror(input_path);
        exit(EXIT_FAILURE);
    }

    int flat_stride = (width + 3) & ~3;
    unsigned char *flat = (unsigned char *) malloc(height * flat_stride);
    flatten_image_yx(flat, flat_stride, image, width, height, stride,
                     params->perc, params->range, params->est_params.batch_size);

    // This is the following line from the python code:
    // flat = np.clip(image[:w, :h]-m[:w, :h]+1, 0, 1)
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            int m = flat[y * flat_stride + x];
            int i = image[y * stride + x];
            int pix = i - m + 255;
            flat[y * flat_stride + x] = pix < 0 ? 0 :
                                        pix > 255 ? 255 : pix; 
        }
    }
    free(image);

    if (params->flat_image_path)
        pgm_save(params->flat_image_path, flat, width, height, flat_stride);

    int threshold = nlbin_estimate_threshold(flat, flat_stride, 
                                             width, height,
                                             &params->est_params);

    int binary_w_in_bytes = (width + 7) >> 3;
    int binary_stride = (binary_w_in_bytes + 3) & ~3;
    unsigned char *binary = (unsigned char *) malloc(height * binary_stride);
    threshold_u8(binary, binary_stride, flat, flat_stride,
                 width, height, threshold);
    
    // invert
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < binary_w_in_bytes; x++)
        {
            binary[y * binary_stride + x] = ~binary[y * binary_stride + x];
        }
    }
    free(flat);

    g4_save(output_path, binary, width, height, binary_stride);
    free(binary);

    if (in != stdin)
        fclose(in);
}

static bool accept_string_option(const char *arg, const char *next,
                                 const char *option, 
                                 const char **value, int *index)
{
    if (strcmp(arg, option))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s", option);
        exit(EXIT_FAILURE);
    }

    *value = next;
    (*index)++;
    return true;
}

static bool accept_int_option(const char *arg, const char *next,
                              const char *option, 
                              int min, int max, int *value, int *index)
{
    if (strcmp(arg, option))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s", option);
        exit(EXIT_FAILURE);
    }

    char *endptr;
    long v = strtol(next, &endptr, 0);
    if (*endptr)
    {
        fprintf(stderr, "Error parsing number for %s option: %s\n", option, next);
        exit(EXIT_FAILURE);
    }

    if (v < min || v > max)
    {
        fprintf(stderr,
                "The value for %s option should be between %d and %d, but found %ld\n", 
                option, min, max, v);
        exit(EXIT_FAILURE);
    }

    *value = (int) v;
    (*index)++;
    return true;
}

static bool accept_double_option(const char *arg, const char *next,
                                 const char *option, 
                                 double min, double max, double *value, int *index)
{
    if (strcmp(arg, option))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s\n", option);
        exit(EXIT_FAILURE);
    }

    char *endptr;
    double v = strtod(next, &endptr);
    if (*endptr)
    {
        fprintf(stderr, "Error parsing number for %s option: %s\n", option, next);
        exit(EXIT_FAILURE);
    }

    if (v < min || v > max)
    {
        fprintf(stderr,
                "The value for %s option should be between %f and %f, but found %lf\n", 
                option, min, max, v);
        exit(EXIT_FAILURE);
    }

    *value = v;
    (*index)++;
    return true;
}

int main(int argc, const char *const *argv)
{
    const char *input_path = NULL;
    const char *output_path = NULL;
    NlbinBinarizerParams params;
    nlbin_params_set_default(&params);

    bool accept_options = true;
    for (int i = 1; i < argc; i++)
    {
        const char *arg = argv[i];
        const char *next = i == argc - 1 ? NULL
                                         : argv[i + 1];
        if (accept_options && *arg == '-' && arg[1])
        {
            if (!strcmp(arg, "--"))
                accept_options = false;
            else if (!accept_string_option(arg, next, "--flat", &params.flat_image_path, &i))
                 if (!accept_int_option(arg, next, "--perc", 0, 100, &params.perc, &i))
                 if (!accept_int_option(arg, next, "--range", 1, 120, &params.range, &i))
                 if (!accept_double_option(arg, next, "--escale", 0, 1e100, &params.est_params.escale, &i))
                 if (!accept_double_option(arg, next, "--border", 0, 0.5, &params.est_params.border, &i))
                 if (!accept_double_option(arg, next, "--low", 0, 100, &params.est_params.low, &i))
                 if (!accept_double_option(arg, next, "--high", 0, 100, &params.est_params.high, &i))
                 if (!accept_double_option(arg, next, "--threshold", 0, 1, &params.est_params.threshold, &i))
                 if (!accept_int_option(arg, next, "--batch-size", 0, INT_MAX, &params.est_params.batch_size, &i))
                 {
                     fprintf(stderr, "Unknown option: %s\n", arg);
                     print_usage(argv[0]);
                     exit(EXIT_FAILURE);
                 }
        }
        else if (!input_path)
            input_path = arg;
        else if (!output_path)
            output_path = arg;
        else
        {
            print_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    } // for arg in argv

    if (!input_path || !output_path)
    {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    nlbin(input_path, output_path, &params);
}
