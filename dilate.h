/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef NLBIN_DILATE_H
#define NLBIN_DILATE_H

int rle_max_dilated_length(int width, int dilation);
int rle_max_length(int width);
int rle_dilate(int *result, const int *runs, int n_runs, int dilation);


typedef struct DilatedLinesWindow DilatedLinesWindow;

DilatedLinesWindow *dlw_new(const unsigned char *src, 
                            int stride, 
                            int width, int height,
                            int x_dilation, int y_dilation);

void dlw_free(DilatedLinesWindow *dlw);

void dlw_slide(DilatedLinesWindow *dlw, int *out_old, int *out_new);

/*
void dilate_2d(unsigned char *dst, int dst_stride,
               const unsigned char *src, int src_stride,
               int width, int height, 
               int x_dilation, int y_dilation);
*/

/**
 * Gather the histogram of 'data' masked by the dilation of 'src'.
 * \param[out] hist         The resulting histogram: an array of length 256
 * \param src               bitmap (PBM-like)
 */
void dilate_2d_histogram(int *hist,
                         const unsigned char *data, int data_stride,
                         const unsigned char *src, int src_stride,
                         int width, int height, 
                         int x_dilation, int y_dilation);

#endif
