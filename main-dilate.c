/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <string.h>
#include <stdlib.h>

#include <g4.h>

#include "dilate.h"

const char *input_path = NULL;
const char *output_path = NULL;
int x_dilation = 1;
int y_dilation = 1;

static void process()
{
    int width, height, stride;
    unsigned char *image = g4_load(input_path, /* alignment: */ 4, 
                                   &width, &height, &stride);
    if (!image)
        return;
    dilate_2d(image, stride, image, stride, width, height,
              x_dilation, y_dilation);

    g4_save(output_path, image, width, height, stride);
    g4_free(image);
}

static bool accept_int_option(const char *opt, const char *next,
                              const char *form1, const char *form2, 
                              int min, int max, int *value, int *index)
{
    if (strcmp(opt, form1) && strcmp(opt, form2))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s/%s\n", form1, form2);
        exit(EXIT_FAILURE);
    }

    char *endptr;
    long v = strtol(next, &endptr, 0);
    if (*endptr)
    {
        fprintf(stderr,
                "Error parsing number for %s/%s option: %s\n", form1, form2, next);
        exit(EXIT_FAILURE);
    }

    if (v < min || v > max)
    {
        fprintf(stderr,
                "The value for %s/%s option should be between %d and %d, but found %ld\n", 
                form1, form2, min, max, v);
        exit(EXIT_FAILURE);
    }

    *value = (int) v;
    (*index)++;
    return true;
}

static void print_usage(const char *prog_name)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s [options] input.tif output.tif\n", prog_name);
    /*
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -r <number> or --rank <number>        Set rank for the rank filter\n");
    fprintf(stderr, "    -p <number> or --percentile <number>  Set rank through a percentile\n");
    fprintf(stderr, "    -w <number> or --window <number>      Set the window size\n");
    fprintf(stderr, "    -b <number> or --batch <number>       Set the batch size\n");
    */
}

int main(int argc, const char *const *argv)
{
    bool accept_options = true;
    for (int i = 1; i < argc; i++)
    {
        const char *arg = argv[i];
        const char *next = i == argc - 1 ? NULL 
                                         : argv[i + 1];
        if (accept_options && *arg == '-')
        {
            if (!strcmp(arg, "--"))
            {
                accept_options = false;
            }
            else if (!accept_int_option(arg, next, "-x", "--x-dilation", 0, INT_MAX, &x_dilation, &i))
                 if (!accept_int_option(arg, next, "-y", "--y-dilation", 0, INT_MAX, &y_dilation, &i))
                    {
                        fprintf(stderr, "Unknown option: %s\n", arg);
                        print_usage(argv[0]);
                        exit(EXIT_FAILURE);
                    }
        }
        else if (!input_path)
            input_path = arg;
        else if (!output_path)
            output_path = arg;
        else
        {
            print_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    if (!input_path || !output_path)
    {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    process();

    return EXIT_SUCCESS;
}
