/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NLBIN_RANK_FILTER_H
#define NLBIN_RANK_FILTER_H


typedef struct RankFilter RankFilter;

RankFilter *rf_new(void /*int rank, int window_size*/);
void rf_free(RankFilter *rf);

void rf_update(RankFilter *rf,
               unsigned char *in,
               int in_len,
               unsigned char *out,
               int out_len);

void rf_load(RankFilter *rf,
             const unsigned char *content,
             int len);

void rf_check(RankFilter *rf,
              const unsigned char *content,
              int len);

unsigned char rf_get(RankFilter *rf);

void flatten_image_xy(unsigned char *dst, int dst_stride,
                      unsigned char *src, int width, int height, int src_stride,
                      int percentile, int window_size, int batch_size);

void flatten_image_yx(unsigned char *dst, int dst_stride,
                      unsigned char *src, int width, int height, int src_stride,
                      int percentile, int window_size, int batch_size);

#endif
