/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NLBIN_BITS_H
#define NLBIN_BITS_H

// Set the given bit in a pbm-like row.
// note: leptonica's uint32-based version of this is called SET_DATA_BIT (arrayaccess.h).
static inline void bit_set(unsigned char *row, int x)
{
    row[x >> 3] |= 0x80 >> (x & 7);
}

// leptonica's uint32-based version is called GET_DATA_BIT (arrayaccess.h)
static inline int bit_get(unsigned char *row, int x)
{
    return row[x >> 3] & (0x80 >> (x & 7));
}

static inline int bits_to_bytes(int n_bits)
{
    return (n_bits + 7) >> 3;
}

#endif
