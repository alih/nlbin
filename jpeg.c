/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include <jpeglib.h>

#include "jpeg.h"

#define ALIGN(X) (((X) + 7) & ~7)

unsigned char *jpeg_read_gray(FILE *f, int *out_width, int *out_height, int *out_stride)
{
    struct jpeg_error_mgr jerr;
    struct jpeg_decompress_struct cinfo;
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
    jpeg_stdio_src(&cinfo, f);
    jpeg_read_header(&cinfo, /* require image: */ TRUE);
    cinfo.out_color_space = JCS_GRAYSCALE;
    jpeg_calc_output_dimensions(&cinfo);

    int w = cinfo.output_width;
    int h = cinfo.output_height;
    int stride = ALIGN(w);

    unsigned char *image = (unsigned char *) malloc(h * stride);
    unsigned char *rowptrs[h];
    for (int i = 0; i < h; i++)
        rowptrs[i] = image + i * stride;

    (void) jpeg_start_decompress(&cinfo);
    
    while (cinfo.output_scanline < h)
    {
        int n_scanlines = jpeg_read_scanlines(&cinfo, rowptrs + cinfo.output_scanline, 
                                              h - cinfo.output_scanline);
        if (!n_scanlines)
            break;
    }

    (void) jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    *out_width = w;
    *out_height = h;
    *out_stride = stride;
    return image;
}
