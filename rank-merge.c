/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rank-flt.h"

struct RankFilter
{
    int window_size;
    int rank;
    unsigned char *items; 
    unsigned char *second_buffer; 
};
typedef RankFilter RF;

static bool is_sorted(const unsigned char *a, int n)
{
    for (int i = 1; i < n; i++)
        if (a[i - 1] > a[i])
            return false;
    return true;
}

RankFilter *rf_new(int rank, int window_size)
{
    RF *rf = (RF *) malloc(sizeof(RF));
    rf->rank = rank;
    rf->window_size = window_size;
    rf->items = (unsigned char *) malloc(window_size);
    rf->second_buffer = (unsigned char *) malloc(window_size);
    return rf; 
}

void rf_free(RankFilter *rf)
{
    free(rf->items);
    free(rf->second_buffer);
    free(rf);
}

static void rf_remove(unsigned char *dst,
                      int dst_len,
                      const unsigned char *src,
                      int src_len,
                      const unsigned char *out, 
                      int out_len)
{
    assert(dst_len >= 0);
    assert(src_len >= 0);
    assert(out_len >= 0);
    assert(dst_len == src_len - out_len);
    assert(is_sorted(src, src_len));
    assert(is_sorted(out, out_len));

    int src_cursor = 0;
    int dst_cursor = 0;
    int out_cursor = 0;
    while (src_cursor < src_len)
    {
        if (out_cursor == out_len)
        {
            memcpy(dst + dst_cursor, src + src_cursor, src_len - src_cursor);
            assert(is_sorted(dst, dst_len));
            return;
        }

        unsigned char next_out = out[out_cursor++];
        while (src[src_cursor] < next_out)
        {
            dst[dst_cursor++] = src[src_cursor++];
            assert(src_cursor < src_len);
        }
        assert(src_cursor < src_len && src[src_cursor] == next_out);
        src_cursor++;
    }
    assert(is_sorted(dst, dst_len));
}

static void rf_insert(unsigned char *dst,
                      int dst_len,
                      const unsigned char *src,
                      int src_len,
                      const unsigned char *in, 
                      int in_len)
{
    assert(dst_len >= 0);
    assert(src_len >= 0);
    assert(in_len >= 0);
    assert(dst_len == src_len + in_len);
    assert(is_sorted(src, src_len));
    assert(is_sorted(in, in_len));

    int src_cursor = 0;
    int dst_cursor = 0;
    int in_cursor = 0;
    while (src_cursor < src_len)
    {
        if (in_cursor == in_len)
        {
            memcpy(dst + dst_cursor, src + src_cursor, src_len - src_cursor);
            assert(is_sorted(dst, dst_len));
            return;
        }

        unsigned char next_in = in[in_cursor++];
        while (src_cursor < src_len && src[src_cursor] < next_in)
            dst[dst_cursor++] = src[src_cursor++];
        dst[dst_cursor++] = next_in;
        assert(dst_cursor == src_cursor + in_cursor);
    }
    while (in_cursor < in_len)
        dst[dst_cursor++] = in[in_cursor++];
    assert(is_sorted(dst, dst_len));
}

static void update_raw(unsigned char *dst,
                       int dst_len,
                       const unsigned char *src,
                       int src_len,
                       const unsigned char *in, 
                       int in_len,
                       const unsigned char *out,
                       int out_len)
{
    assert(dst_len >= 0);
    assert(src_len >= 0);
    assert(in_len >= 0);
    assert(out_len >= 0);
    assert(dst_len == src_len + in_len - out_len);
    assert(is_sorted(src, src_len));
    assert(is_sorted(in, in_len));
    assert(is_sorted(out, out_len));

    int in_cursor = 0;
    int out_cursor = 0;
    int dst_cursor = 0;
    int src_cursor = 0;

    while (src_cursor < src_len)
    {
        if (in_cursor == in_len)
        {
            rf_remove(dst + dst_cursor, dst_len - dst_cursor, 
                      src + src_cursor, src_len - src_cursor, 
                      out + out_cursor, out_len - out_cursor);
            return;
        } 
        if (out_cursor == out_len)
        {
            rf_insert(dst + dst_cursor, dst_len - dst_cursor,
                      src + src_cursor, src_len - src_cursor, 
                      in + in_cursor, in_len - in_cursor);
            return;
        } 

        unsigned char next_in = in[in_cursor];
        unsigned char next_out = out[out_cursor];
        if (next_in == next_out)
        {
            in_cursor++;
            out_cursor++;
            continue;
        }

        if (next_in < next_out)
        {
            while (src_cursor < src_len && src[src_cursor] < next_in)
                dst[dst_cursor++] = src[src_cursor++];
            dst[dst_cursor++] = next_in;
            in_cursor++;
        }
        else
        {
            while (src[src_cursor] < next_out)
                dst[dst_cursor++] = src[src_cursor++];
            assert(src_cursor < src_len && src[src_cursor] == next_out);
            out_cursor++;
            src_cursor++;
        }
    }
    while (in_cursor < in_len)
    {
        unsigned char next_in = in[in_cursor];
        if (out_cursor < out_len && out[out_cursor] == next_in)
            out_cursor++;
        else
            dst[dst_cursor++] = next_in;
        in_cursor++;
    }
    assert(dst_cursor == dst_len);
    assert(is_sorted(dst, dst_len));
}

// binarysort() taken from Python's timsort
/* binarysort is the best method for sorting small arrays: it does
   few compares, but can do data movement quadratic in the number of
   elements.
   [lo, hi) is a contiguous slice of a list, and is sorted via
   binary insertion.  This sort is stable.
   On entry, must have lo <= start <= hi, and that [lo, start) is already
   sorted (pass start == lo if you don't know!).
   If islt() complains return -1, else 0.
   Even in case of error, the output slice will be some permutation of
   the input (nothing is lost or duplicated).
*/
static void binarysort(unsigned char *lo, unsigned char *hi, unsigned char *start)
{
    unsigned char *l, *p, *r;
    unsigned char pivot;

    assert(lo <= start && start <= hi);
    /* assert [lo, start) is sorted */
    if (lo == start)
        ++start;
    for (; start < hi; ++start) {
        /* set l to where *start belongs */
        l = lo;
        r = start;
        pivot = *r;
        /* Invariants:
         * pivot >= all in [lo, l).
         * pivot  < all in [r, start).
         * The second is vacuously true at the start.
         */
        assert(l < r);
        do {
            p = l + ((r - l) >> 1);
            if (pivot < *p)
                r = p;
            else
                l = p+1;
        } while (l < r);
        assert(l == r);
        /* The invariants still hold, so pivot >= all in [lo, l) and
           pivot < all in [l, start), so pivot belongs at l.  Note
           that if there are elements equal to pivot, l points to the
           first slot after them -- that's why this sort is stable.
           Slide over to make room.
           Caution: using memmove is much slower under MSVC 5;
           we're not usually moving many slots. */
        for (p = start; p > l; --p)
            *p = *(p-1);
        *l = pivot;
    }
}

void rf_update(RankFilter *rf,
               unsigned char *in,
               int in_len,
               unsigned char *out,
               int out_len)
{
    assert(in_len == out_len);

    binarysort(in, in + in_len, in);
    binarysort(out, out + out_len, out);
    update_raw(rf->second_buffer, rf->window_size,
               rf->items, rf->window_size,
               in, in_len,
               out, out_len);

    unsigned char *tmp = rf->second_buffer;
    rf->second_buffer = rf->items;
    rf->items = tmp; 
}

void rf_load(RankFilter *rf,
             const unsigned char *content,
             int len)
{
    assert(rf->window_size == len);
    memcpy(rf->items, content, len);
    binarysort(rf->items, rf->items + len, rf->items);
}

void rf_check(RankFilter *rf,
              const unsigned char *content,
              int len)
{
    unsigned char buf[len];
    assert(rf->window_size == len);
    memcpy(buf, content, len);
    binarysort(buf, buf + len, buf);
    assert(!memcmp(buf, rf->items, len));
}

unsigned char rf_get(RankFilter *rf)
{
    return rf->items[rf->rank];
}
