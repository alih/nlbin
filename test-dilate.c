/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // for testing

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <g4.h>
#include "dilate.h"
#include "bits.h"

// testing x dilation {{{

/*
 * Dilate using counters.
 *
 * \param[out]  dst         Resulting array of bits, unpacked (1 - black, 0 - white) 
 * \param[out]  src         Input array of bits, unpacked (nonzero - black, zero - white) 
 * \param       n           Length of src and dst.
 * \param       dilation    How much to dilate
 */
static void dumb_dilate(unsigned char *dst, const unsigned char *src, int n, int dilation)
{
    int counters[n];
    memset(counters, 0, sizeof(counters));
    int guard_left = 0;

    // fill the counters
    for (int i = 0; i < n; i++)
    {
        if (!src[i])
            continue;

        int left = i - dilation;
        if (left < 0)
            guard_left++;
        else
            counters[left]++;

        int right = i + dilation + 1;
        if (right < n)
            counters[right]--;
    }

    // integrate
    int sum = guard_left;
    for (int i = 0; i < n; i++)
    {
        sum += counters[i];
        assert(sum >= 0);
        dst[i] = sum ? 1 : 0;
    }
}

static void repack(unsigned char *result, int w, int packed)
{
    memset(result, 0, bits_to_bytes(w));
    for (int x = 0; x < w; x++)
    {
        if (packed & (1 << x))
            bit_set(result, x);
    }
}

static void unpack(unsigned char *dst, unsigned char *src, int n)
{
    memset(dst, 0, n);
    for (int i = 0; i < n; i++)
        if (bit_get(src, i))
            dst[i] = 1;
}

static void pack(unsigned char *dst, unsigned char *src, int n)
{
    memset(dst, 0, bits_to_bytes(n));
    for (int i = 0; i < n; i++)
        if (src[i])
            bit_set(dst, i);
}

static void test_dilate(unsigned char *pixels, int w, int dilation)
{
    unsigned char buf[w];
    unpack(buf, pixels, w); 

    int rle[rle_max_dilated_length(w, 0)];
    int n_runs = g4_rle_extract(rle, pixels, w);

    unsigned char dilated[w];
    dumb_dilate(dilated, buf, w, dilation);

    unsigned char dilated_packed[bits_to_bytes(w)];
    pack(dilated_packed, dilated, w);

    int rle_dilated[rle_max_dilated_length(w, dilation)];
    int n_runs_dilated = g4_rle_extract(rle_dilated, dilated_packed, w);

    int result_rle[rle_max_dilated_length(w, dilation)];
    int n_result_runs = rle_dilate(result_rle, rle, n_runs, dilation);

    assert(n_runs_dilated == n_result_runs);
    assert(!memcmp(rle_dilated, result_rle, n_result_runs * sizeof(int)));
}

static void test_dilate_all(int n, int dilation)
{
    for (int i = 0, i_cap = 1 << n; i < i_cap; i++)
    {
        unsigned char buf[sizeof(int)];

        repack(buf, n, i);
        test_dilate(buf, n, dilation);
    }
}

// testing x dilation }}}

/**
 * Dilate an image with y_dilation so big that all the rows of the result will be the same.
 */
/*void rle_dilate_y_total(unsigned char *dst, int dst_stride,
                        const unsigned char *src, int src_stride,
                        int width, int height,
                        int x_dilation)
{
    int counters[width + 1];
    int rle[rle_max_length(width)];
    int dilated_rle[rle_max_dilated_length(width, x_dilation)];
    memset(counters, 0, sizeof(counters));

    for (int i = 0; i < h; i++)
    {
        int n_runs = g4_rle_extract(rle, src + i * src_stride, width);
        int n_dilated_runs = rle_dilate(dilated_rle, rle, n_runs, x_dilation);
        render_rle_to_counters(counters, width, dilated_rle, n_dilated_runs, 1);
    }

    extract_dilation_from_counters(dst, counters, width);
    for (int i = 1; i < h; i++)
        memcpy(dst + i * dst_stride, dst, (width + 7) >> 3);
}*/


// In the narrow dilation, the image is more narrow than the dilation window.
// There are 4 phases as the window moves:
//    preload
//    growing
//    staying
//    shrinking
/*void rle_dilate_y_narrow(int *runs, int *offsets, int *n_runs, int w, int h, int dilation)
{
    assert(h > dilation + 1);
    int dilation_window = 2 * dilation + 1;
    assert(h <= dilation_window);
    
    // preload phase
    for (int i = 0; i <= dilation; i++)
        render_rle_to_counters(XXXXXXXXXXXXXXX);
        render_rle_to_counters(counters, width, dilated_rle, n_dilated_runs, 1);

    int y = 0; // destination index
    // growing phase
    // the last dst row is h - 1 - dilation (and is > 0)
    for (int i = 0; i < XXXXXXX
    // growing ends with the window bottom hitting image bottom
    
    // staying phase
    // the last row is y == dilation
    
    // shrinking phase

}*/

// In the wide dilation, the image is greater than the dilation window.
/*void rle_dilate_y_wide(int *runs, int *offsets, int *n_runs, int w, int h, int dilation)
{
    int dilation_window = 2 * dilation + 1;
    assert(h >= dilation_window);

    // preload with dilation + 1
    for (int i = 0; i <= dilation; i++)
        render_rle_to_counters(counters, width, XXX);


    // preload
    
    // growing

    // sliding
    for (int y_cap = h - dilation; y < y_cap; y++)
    {
    }

    // shrinking
    
}
*/

// testing 2d dilation {{{



// testing 2d dilation }}}


int main()
{
    for (int n = 0; n <= 14; n++)
        for (int d = 0; d <= n; d++)
            test_dilate_all(n, d);
}

// vim: set fdm=marker:
