if [ -f libg4/configure ]
then :
else
    echo """
        Looks like the git submodules were not cloned.
        If so, these commands need to be run:
            git submodule init
            git submodule update
    """

    while true; do
        read -p "Do you wish to do that now (y/n)? " yn
        case $yn in
            [Yy]* ) git submodule init; git submodule update; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
fi

cd libg4
./configure || exit 1
./waf || exit 1
cd ..

ninja || exit 1

nlbin=$(dirname $0)/nlbin
if [ -x $nlbin ]
then
    echo
    echo Build successful, the executable is $nlbin.
    echo Installation is not supported anyway.
fi
