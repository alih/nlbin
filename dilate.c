/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <g4.h>
#include "dilate.h"
#include "bits.h"

int rle_max_length(int width)
{
    return width + 2;
}

int rle_max_dilated_length(int width, int dilation)
{
    int min_black = 2 * dilation + 1;
    // a full white-black pair is at least min_black + 1
    // however, the first pair might have the white run of 0
    // and the last pair might have black run of 0
    // so
    //    width >= (min_black + 1) * (max_n_pairs - 2) + min_black + 1
    //    width >= (min_black + 1) * max_n_pairs - min_black - 1 
    //    width + min_black + 1 >= (min_black + 1) * max_n_pairs
    return 2 * width / (min_black + 1) + 2;
}

/*
void threshold_and_extract_rle_int(int *runs,
                                   const int *a, const int *b,
                                   int n)
{

}
*/

/*
 * Returns an index of a pixel greater or equal to the threshold,
 * of -1 if not found. 
 */

#if 0
static int dumb_threshold_look_int(const int *line, int threshold, int n)
{
    for (int i = 0; i < n; i++)
        if (line[i] >= threshold)
            return i;
    return -1;
}

static int threshold_look_int(const int *line, int threshold, int n)
{
    int m = n & ~7;

    for (int i = m; i < n; i++)
        if (line[i] >= threshold)
            return i;

    for (int i = m - 8; i >= 0; i -= 8)
        for (int j = 0; j < 8; j++)
            if (line[i + j] >= threshold)
                return i + j;

    return -1;
}
#endif

// rle_dilate (1d dilation) {{{
int rle_dilate(int *result, const int *runs, int n_runs, int dilation)
{
    assert(n_runs >= 0);
    assert(!(n_runs & 1));

    if (!n_runs)
        return 0;

    if (n_runs == 2 && !runs[1])
    {
        // pure white
        result[0] = runs[0];
        result[1] = 0;
        return 2;
    }

    // this is the minimum length of a white run 
    //     in the middle of the source line
    //         to still remain a white run after dilation.
    int min_src_middle_white_run = 2 * dilation + 1;
    int min_src_border_white_run = dilation + 1;

    int first_run = *runs - dilation;
    int black_acc = dilation;
    if (first_run < 0)
    {
        first_run = 0;
        black_acc = *runs;
    }

    assert(first_run + black_acc == *runs);
    *result = first_run;

    if (n_runs == 2)
    {
        result[1] = black_acc + runs[1];
        return 2;
    }

    int i_cap = runs[n_runs - 1] ? n_runs 
                                 : n_runs - 2;
    int i = 1;
    int j = 1;
    // invariant: sum(result[:j]) + black_acc == sum(runs[:i])
    while (1) // the main loop
    {
        // get the next black run
        assert(i & 1);
        assert(j & 1);

        // the black loop (the black run building loop)
        // invariant: sum(result[:j]) + black_run == sum(runs[:i])
        int black_run = black_acc + runs[i++]; 
        while (i < i_cap && runs[i] < min_src_middle_white_run)
        {
            black_run += runs[i] + runs[i + 1];
            i += 2;
        }

        assert(!(i & 1));
        if (i >= i_cap)
        {
            // the end
            assert(i == i_cap);
            if (i_cap == n_runs)
            {
                // black end
                result[j++] = black_run; // sum(result[:j]) == sum(runs[:i]) == sum(runs)
                return j;
            }

            if (runs[i] < min_src_border_white_run)
            {
                // white end chewed by dilation
                result[j++] = black_run + runs[i];
                return j;
            }

            // white end surviving dilation
            result[j++] = black_run + dilation;
            result[j++] = runs[i] - dilation;
            result[j++] = 0;
            return j;
        }

        // get the next white run
        assert(runs[i] >= min_src_middle_white_run); // because we're out of the black run building loop
        result[j++] = black_run + dilation;
        assert(!(j & 1));
        result[j++] = runs[i++] - 2 * dilation;
        black_acc = dilation;
    } // main loop
}
// RLE line dilation }}}

// DilatedLinesWindow {{{

struct DilatedLinesWindow
{
    const unsigned char *src; // not owned
    int *runs; // owned
    int *n_runs;
    int src_width;
    int src_height; 
    int src_stride;
    int span; // == y_dilation
    int window_height; // == min(2 * span + 1, src_height)
    int stride; // of runs
    int cursor;
    int fill;
    int y;
    int x_dilation;
};

/*
 * Extract a dilated RLE line from the specified source row.
 */
static int dlw_extract_raw(const DilatedLinesWindow *dlw, int *result, int y)
{
    int runs[rle_max_length(dlw->src_width)];
    int n_runs = g4_rle_extract(runs, dlw->src + y * dlw->src_stride, dlw->src_width);
    return rle_dilate(result, runs, n_runs, dlw->x_dilation);
}

static void dlw_extract(DilatedLinesWindow *dlw, int i, int y)
{
    assert(i >= 0);
    assert(i < dlw->window_height);
    assert(y >= 0);
    assert(y < dlw->src_height);
    dlw->n_runs[i] = dlw_extract_raw(dlw, dlw->runs + i * dlw->stride, y);
}

static void dlw_preload(DilatedLinesWindow *dlw)
{
    int i_cap = dlw->span + 1;
    if (i_cap > dlw->src_height)
        i_cap = dlw->src_height;

    for (int i = 0; i < i_cap; i++)
        dlw_extract(dlw, i, i);

    dlw->fill = i_cap;
}

DilatedLinesWindow *dlw_new(const unsigned char *src, int stride, int width, int height,
                            int x_dilation, int y_dilation)
{
    DilatedLinesWindow *dlw = (DilatedLinesWindow *) malloc(sizeof(DilatedLinesWindow));
    dlw->src = src;
    dlw->src_stride = stride;
    dlw->src_width = width;
    dlw->src_height = height;
    dlw->span = y_dilation;
    dlw->window_height = 2 * y_dilation + 1;
    if (dlw->window_height > height)
        dlw->window_height = height;
    dlw->stride = rle_max_dilated_length(width, x_dilation);
    dlw->cursor = 0;
    dlw->runs = (int *) malloc(dlw->window_height * dlw->stride * sizeof(int));
    dlw->n_runs = (int *) malloc(dlw->window_height * sizeof(int));
    dlw->y = 0;
    dlw->x_dilation = x_dilation;
    dlw_preload(dlw);
    return dlw;
}

void dlw_free(DilatedLinesWindow *dlw)
{
    free(dlw->runs);
    free(dlw->n_runs);
    free(dlw);
}

static void dlw_advance_cursor(DilatedLinesWindow *dlw)
{
    dlw->cursor++;
    if (dlw->cursor == dlw->window_height)
        dlw->cursor = 0;
}

void dlw_slide(DilatedLinesWindow *dlw, int *out_old, int *out_new)
{
    int old_row_y = dlw->y - dlw->span; // this row gets out of the window
    dlw->y++;
    int new_row_y = dlw->y + dlw->span; // this row gets into the window

    if (new_row_y > dlw->src_height)
    {
        *out_new = -1;
        // we don't need to add new data
        if (old_row_y < 0)
        {
            *out_old = -1;
            return;
        }

        // but we need to take out old data
        dlw->fill--;
        assert(dlw->fill > 0);
        *out_old = dlw->cursor;
        dlw_advance_cursor(dlw);
        return;
    }

    int i = dlw->cursor + dlw->fill;
    if (i >= dlw->window_height)
        i -= dlw->window_height;

    *out_new = i;

    if (old_row_y < 0)
    {
        *out_old = -1;
        dlw->fill++;
        return;
    }

    *out_old = dlw->cursor;
    dlw_advance_cursor(dlw);
    return;
}

// DilatedLinesWindow }}}

#if 0
void threshold_and_dilate_int(int *runs,
                              const int *pixels, 
                              int n,
                              int threshold, 
                              int dilation)
{
    assert(dilation >= 0);
    if (!dilation)
    {
        threshold_and_extract_rle_int(runs, a, b, n);
        return;
    }
    int x = 0; // the x of a result pixel whose color we don't know yet
    while (x < n)
    {
        // we need to know the color of x
        int right = x + dilation;
        int to_look = right <= n ? dilation 
                                 : n - x;
        int ahead = threshold_look_int(runs + x, threshold, to_look);
        
        // black run building loop
        
    }
}

#endif

// \param counters    an array of at least width + 2 items
static void render_rle_to_counters(
        int *counters, int width, 
        const int *runs, int n_runs,
        int adjustment)
{
    (void) width; // only needed for the asserts
    assert(!(n_runs & 1));
    int x = 0;
    for (int i = 0; i < n_runs; i += 2)
    {
        x += runs[i];
        assert(x <= width);
        counters[x] += adjustment;
        x += runs[i + 1];
        assert(x <= width);
        counters[x] -= adjustment;
    }
}

static void extract_dilation_from_counters(unsigned char *result, const int *counters, int w)
{
    memset(result, 0, bits_to_bytes(w));
    int sum = 0;
    for (int i = 0; i < w; i++)
    {
        sum += counters[i];
        if (sum)
            bit_set(result, i);
    }
}

static void gather_histogram_from_dilation_counters(int *hist,
                                                    const int *counters,
                                                    const unsigned char *data,
                                                    int w)
{
    int sum = 0;
    for (int i = 0; i < w; i++)
    {
        sum += counters[i];
        if (sum)
            hist[data[i]]++;
    }
}
/*
static void print_counters(const int *counters, int width)
{
    printf("counters: ");
    for (int i = 0; i < width; i++)
        printf("%d ", counters[i]);
    printf("| %d\n", counters[width]);
}*/

void dilate_2d_histogram(int *hist, //unsigned char *dst, int dst_stride,
                         const unsigned char *data, int data_stride,
                         const unsigned char *src, int src_stride,
                         int width, int height, 
                         int x_dilation, int y_dilation)
{
    int counters[width + 1];
    memset(counters, 0, sizeof(counters));
    DilatedLinesWindow *dlw = dlw_new(src, src_stride,
                                      width, height,
                                      x_dilation, y_dilation);

    // init the counters 
    for (int i = 0; i < dlw->fill; i++)
    {
        render_rle_to_counters(counters, width,
                               dlw->runs + i * dlw->stride, dlw->n_runs[i],
                               1);
    }

    while (1)
    {
        //extract_dilation_from_counters(dst + dlw->y * dst_stride, 
        //                               counters, width);
        gather_histogram_from_dilation_counters(hist, counters, 
                                                data + dlw->y * data_stride, width);

        if (dlw->y == height - 1)
            break;

        int old;
        int new;
        dlw_slide(dlw, &old, &new); 

        if (old >= 0)
        {
            render_rle_to_counters(counters, width, 
                                   dlw->runs + old * dlw->stride, 
                                   dlw->n_runs[old], -1);
        }

        if (new >= 0)
        {
            dlw_extract(dlw, new, dlw->y);
            render_rle_to_counters(counters, width, 
                                   dlw->runs + new * dlw->stride, 
                                   dlw->n_runs[new], 1);
        }
    }

    dlw_free(dlw);
    
    /*
    if (height <= 2 * y_dilation + 1)
        return rle_dilate_y_narrow(XXXXXX);

    return rle_dilate_y_wide(XXXXXXXX);*/
}



// vim: set fdm=marker:
