/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "box-flt.h"
#include "seesaw.h"

//static int min(int a, int b) { return a < b ? a : b; }
//static int min3(int a, int b, int c) { return min(a, min(b, c)); }

// initializes the box
#define BOX_SUM_ONCE_BODY \
    SeesawSegment seg[SEESAW_MAX_SEGMENTS]; \
    int c = seesaw_segment(seg, len, initial_left, initial_right); \
    int sum = 0; \
    for (int i = 0; i < c; i++) \
    { \
        int s = 0; \
        for (int j = seg[i].start; j < seg[i].end; j++) \
            s += src[j]; \
        sum += s * seg[i].coef; \
    } \
    return sum; 
int box_sum_u8_once(const unsigned char *src, int len, 
                    int initial_left, int initial_right)
{
    BOX_SUM_ONCE_BODY
}
int box_sum_int_once(const int *src, int len, 
                     int initial_left, int initial_right)
{
    BOX_SUM_ONCE_BODY
}
#undef BOX_SUM_ONCE_BODY

#define BOX_SUM_BODY(SUM_ONCE_FUNC) \
    if (len <= 0) \
        return; \
\
    assert(initial_left <= initial_right); \
\
    int sum = SUM_ONCE_FUNC(src, len, initial_left, initial_right); \
\
    SeesawUpdate upd[SEESAW_MAX_UPDATES]; \
    int c = seesaw_schedule_updates(upd, len, initial_left, initial_right); \
    int i = 0; \
    for (int u = 0; u < c; u++) \
    { \
        int l = upd[u].left; \
        int r = upd[u].right; \
        int cap = upd[u].cap; \
\
        switch ((upd[u].left_backwards << 1) + upd[u].right_backwards) \
        { \
            case 0: \
                while (i < cap) { dst[i++] = sum; sum += src[r++] - src[l++]; }  \
                break; \
            case 1: \
                while (i < cap) { dst[i++] = sum; sum += src[r--] - src[l++]; }  \
                break; \
            case 2: \
                while (i < cap) { dst[i++] = sum; sum += src[r++] - src[l--]; }  \
                break; \
            case 3: \
                while (i < cap) { dst[i++] = sum; sum += src[r--] - src[l--]; }  \
                break; \
        } \
    }

void box_sum_u8(int *dst, 
                const unsigned char *src, int len, 
                int initial_left, 
                int initial_right)
{
    BOX_SUM_BODY(box_sum_u8_once)
}

void box_sum_int(int *dst, 
                 const int *src, int len, 
                 int initial_left, 
                 int initial_right)
{
    BOX_SUM_BODY(box_sum_int_once)
}
