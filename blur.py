__all__ = ["blur_1d", "blur"]

from ctypes import c_int, c_double
import numpy
import os
from numpy.ctypeslib import ndpointer

here = os.path.dirname(__file__)
lib = numpy.ctypeslib.load_library('libblur.so', here)

lib.gaussian_blur_1d_u8.restype = None
lib.gaussian_blur_1d_u8.argtypes = [
        ndpointer(numpy.uint8, ndim=1, flags=('writeable')),
        c_int,
        ndpointer(numpy.uint8, ndim=1, flags=('contiguous')),
        c_int,
        c_double]

lib.gaussian_blur_u8.restype = None
lib.gaussian_blur_u8.argtypes = [
        ndpointer(numpy.uint8, ndim=2, flags=('writeable')),
        c_int,
        ndpointer(numpy.uint8, ndim=2),
        c_int, c_int, c_int,
        c_double, c_double,
        c_int]

lib.gaussian_blur_int.restype = None
lib.gaussian_blur_int.argtypes = [
        ndpointer(numpy.int32, ndim=2, flags=('writeable')),
        c_int,
        ndpointer(numpy.int32, ndim=2),
        c_int, c_int, c_int,
        c_double, c_double,
        c_int]

def c_ify(a):
    if a.strides[-1] == a.dtype.itemsize:
        return a
    return numpy.ascontiguousarray(a)

def blur_1d(src, sigma):
    dst = numpy.empty_like(src)
    lib.gaussian_blur_1d_u8(dst, 1,
                            src, len(src), sigma)
    return dst

def blur(src, sigma, batch_size=32):
    dst = numpy.empty_like(src)
    src = c_ify(src)
    if src.dtype.itemsize == 1:
        lib.gaussian_blur_u8(dst, dst.strides[0],
                             src, src.shape[1], src.shape[0],
                             src.strides[0],
                             sigma, sigma, batch_size)
    else:
        lib.gaussian_blur_int(dst, dst.strides[0],
                             src, src.shape[1], src.shape[0],
                             src.strides[0],
                             sigma, sigma, batch_size)

    return dst
