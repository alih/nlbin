/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "box-flt.h"
#include "blur-flt.h"


// [1] http://www.w3.org/TR/SVG11/filters.html#feGaussianBlur
// XXX only works reasonably for sigma at least 2

static int box_size_from_sigma(double sigma)
{
    return (int) (sigma * 1.88 + 0.5); // 1.88 ~= 3 * sqrt(2 * pi) / 4
}

static int norm_by_box_size(int d)
{
    return d * d * (d + 1 - (d & 1));
}

typedef enum
{
    TYPE_UCHAR,
    TYPE_INT
} Type;

static void box_sum(Type src_type, int *dst, 
                    const void *src, int len, 
                    int left, int right)
{
    switch (src_type)
    {
        case TYPE_UCHAR: 
            box_sum_u8(dst, (const unsigned char *) src, len, left, right);
            break;
        case TYPE_INT:
            box_sum_int(dst, (const int *) src, len, left, right);
            break;
    }
}

// requires a workspace of len integers
// returns unnormalized result
static void gaussian_blur_raw(Type src_type,
                              int *dst, int *workspace, 
                              const void *src, int len, 
                              int d)
{
    if (d & 1)
    {
        // ... if d is odd, use three box-blurs of size 'd', 
        //     centered on the output pixel. [1]
        int span = d / 2;
        int left = - span;
        int right = span + 1;

        box_sum(src_type, dst, src, len, left, right);
        box_sum_int(workspace, dst, len, left, right);
        box_sum_int(dst, workspace, len, left, right);
    }

    // ... if d is even, two box-blurs of size 'd' 
    // (the first one centered on the pixel boundary 
    //      between the output pixel and the one to the left, 
    //  the second one centered on the pixel boundary 
    //      between the output pixel and the one to the right)
    //  and one box blur of size 'd+1' centered on the output pixel. [1]
    int span = d / 2;
    box_sum(src_type, dst, src, len, - span, span);
    box_sum_int(workspace, dst, len, - span + 1, span + 1);
    box_sum_int(dst, workspace, len, - span, span + 1);
}

static void gaussian_blur_u8_normalized(unsigned char *dst, int dst_stride,
                                        int *workspace1, int *workspace2, 
                                        const unsigned char *src, int len, double sigma)
{
    int d = box_size_from_sigma(sigma);
    gaussian_blur_raw(TYPE_UCHAR, workspace1, workspace2, src, len, d);
    
    // normalize
    int norm = norm_by_box_size(d);
    int shift = norm / 2;
    if (dst_stride == 1)
    {
        for (int i = 0; i < len; i++)
            dst[i] = (workspace1[i] + shift) / norm;
    }
    else
    {
        for (int i = 0; i < len; i++)
            dst[i * dst_stride] = (workspace1[i] + shift) / norm;
    }
}

void gaussian_blur_1d_u8(unsigned char *dst, int dst_stride,
                         const unsigned char *src, int len, double sigma)
{
    int *workspace = (int *) malloc(len * 2 * sizeof(int));
    gaussian_blur_u8_normalized(dst, dst_stride, workspace, workspace + len, src, len, sigma);
    free(workspace);
}


#define BLUR_BATCH_BODY(SRC_TYPE) \
    for (int i = 0; i < batch_size; i++) \
    { \
        gaussian_blur_raw(SRC_TYPE, \
                          workspace1 + i * width, \
                          workspace2, \
                          src + i * src_stride, \
                          width, d); \
    } \
    /* norm and transpose */ \
    int shift = norm / 2; \
    for (int y = 0; y < width; y++) \
        for (int i = 0; i < batch_size; i++) \
            dst[y * dst_stride + i] = (workspace1[i * width + y] + shift) / norm; 

static void gaussian_blur_t_batch_u8_raw
    (unsigned char *dst, int dst_stride,
     const unsigned char *src, int src_stride,
     int width, int batch_size, 
     int *workspace1, // width * batch_size
     int *workspace2, // width
     int d, int norm)
{
    BLUR_BATCH_BODY(TYPE_UCHAR)
}
static void gaussian_blur_t_batch_int_raw
    (int *dst, int dst_stride,
     const int *src, int src_stride,
     int width, int batch_size, 
     int *workspace1, // width * batch_size
     int *workspace2, // width
     int d, int norm)
{
    BLUR_BATCH_BODY(TYPE_INT)
}

#define BLUR_T_BODY(BLUR_BATCH_FUNC) \
    int d = box_size_from_sigma(sigma); \
    int norm = norm_by_box_size(d); \
\
    int y = 0; \
    while (y < height) \
    { \
        if (y + batch_size >= height) \
            batch_size = height - y; \
    \
        BLUR_BATCH_FUNC(dst + y, dst_stride,  \
                        src + y * src_stride, src_stride, \
                        width, batch_size, \
                        workspace1, workspace2, d, norm); \
\
        y += batch_size; \
    }
static void gaussian_blur_x_t_u8(unsigned char *dst, int dst_stride,
                                 const unsigned char *src, int width, int height,
                                 int src_stride,
                                 int *workspace1, // width * batch_size
                                 int *workspace2, // width
                                 double sigma,
                                 int batch_size)
{
    BLUR_T_BODY(gaussian_blur_t_batch_u8_raw)
}
static void gaussian_blur_x_t_int(int *dst, int dst_stride,
                                  const int *src, int width, int height,
                                  int src_stride,
                                  int *workspace1, // width * batch_size
                                  int *workspace2, // width
                                  double sigma,
                                  int batch_size)
{
    BLUR_T_BODY(gaussian_blur_t_batch_int_raw)
}

#define BLUR_BODY(BLUR_T_FUNC, T) \
    int m = width > height ? width : height; \
    int *workspace1 = (int *) malloc((batch_size + 1) * m * sizeof(int) + width * height * sizeof(T)); \
    T *buf = (T *) (workspace1 + (batch_size + 1) * m); \
    int *workspace2 = workspace1 + batch_size * m; \
\
    BLUR_T_FUNC(buf, /* buf_stride: */ height, \
                 src, width, height, src_stride, \
                 workspace1, workspace2, \
                 x_sigma, batch_size); \
    BLUR_T_FUNC(dst, dst_stride, \
                 buf, height, width, height, \
                 workspace1, workspace2, \
                 y_sigma, batch_size); \
    free(workspace1);

void gaussian_blur_u8(unsigned char *dst, int dst_stride,
                      const unsigned char *src, int width, int height,
                      int src_stride,
                      double x_sigma, double y_sigma, 
                      int batch_size)
{
    BLUR_BODY(gaussian_blur_x_t_u8, unsigned char)
}

void gaussian_blur_int(int *dst, int dst_stride,
                      const int *src, int width, int height,
                      int src_stride,
                      double x_sigma, double y_sigma, 
                      int batch_size)
{
    BLUR_BODY(gaussian_blur_x_t_int, int)
}
