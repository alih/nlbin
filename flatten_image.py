__all__ = ['flatten_image']

import ctypes
import numpy
import os

here = os.path.dirname(__file__)
lib = numpy.ctypeslib.load_library('librf.so', here)
lib.flatten_image_yx.restype = None
lib.flatten_image_yx.argtypes = [
            numpy.ctypeslib.ndpointer(numpy.uint8, ndim=2, flags='writeable'),
            ctypes.c_int,
            numpy.ctypeslib.ndpointer(numpy.uint8, ndim=2),
            ctypes.c_int, # width
            ctypes.c_int, # height
            ctypes.c_int, # src_stride
            ctypes.c_int, # percentile
            ctypes.c_int,
            ctypes.c_int] # range

def c_ify(a):
    if a.strides[-1] == a.dtype.itemsize:
        return a
    return numpy.ascontiguousarray(a)

def flatten_image(a, percentile, range, batch_size=32):
    if a.dtype.itemsize != 1:
        raise ValueError("only byte arrays are supported")
    a = c_ify(a)
    if percentile < -100:
        raise ValueError("percentile should be >= -100")
    if percentile < 0:
        percentile += 100
    result = numpy.empty_like(a)
    lib.flatten_image_yx(result, result.strides[0], a, a.shape[1], a.shape[0],
                      a.strides[0], percentile, range, batch_size)
    return result

if __name__ == '__main__':
    from scipy.ndimage.filters import percentile_filter
    import sys
    from timeit import timeit
    from PIL import Image

    def slow_flatten_image(img, percentile, range):
        return percentile_filter(
                   percentile_filter(
                       img, 
                       percentile=percentile, 
                       size=(range, 2)),
                   percentile=percentile, 
                   size=(2, range))

    if len(sys.argv) < 2:
        print("Usage: %s input_image" % sys.argv[0])
        sys.exit(1)

    img = numpy.asarray(Image.open(sys.argv[1]))
    if img.ndim > 2:
        img = img.mean(axis=2).astype(numpy.uint8)
    percentile = 80
    range = 40

    gt = None
    def run_slow():
        global gt
        gt = slow_flatten_image(img, percentile=percentile, range=range)

    my = None
    def run_fast():
        global my
        my = flatten_image(img, percentile=percentile, range=range)

    slow_time = timeit(run_slow, number=1)
    print("using percentile_filter from scipy: %f sec" % slow_time)
    fast_time = timeit(run_fast, number=1)
    print("using flatten_image() written in C: %f sec" % fast_time )
    print("speedup: %f times" % (slow_time / fast_time))

    assert((gt == my).all())
