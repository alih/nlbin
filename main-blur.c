/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "blur-flt.h"
#include "pgm.h"

const char *input_path = NULL;
const char *output_path = NULL;

static void process()
{
    int width, height;
    unsigned char *image = pgm_load(input_path, &width, &height);
    unsigned char *buf = (unsigned char *) malloc(width * height);
    int stride = width;

    
    gaussian_blur_u8(buf, stride,
                     image, width, height, stride,
                     20, 20, 32);

    pgm_save(output_path, buf, width, height, stride);
    free(image);
    free(buf);
}

static void print_usage(const char *prog_name)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s input.pgm output.pgm\n", prog_name);
}

int main(int argc, const char *const *argv)
{
    for (int i = 1; i < argc; i++)
    {
        const char *arg = argv[i];

        if (!input_path)
            input_path = arg;
        else if (!output_path)
            output_path = arg;
        else
        {
            print_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    if (!input_path || !output_path)
    {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    process();

    return EXIT_SUCCESS;
}
