/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "threshold.h"

#define THRESHOLD_8(NAME, TYPE) \
static unsigned char NAME(const TYPE *src, TYPE threshold) \
{ \
    unsigned char result = 0; \
    for (int i = 0; i < 8; i++) \
    { \
        if (src[i] >= threshold) \
            result += 1 << (7 - i); \
    } \
    return result; \
}

THRESHOLD_8(threshold_8_u8, unsigned char)
THRESHOLD_8(threshold_8_int, int)

#define THRESHOLD_INCOMPLETE(NAME, TYPE) \
static unsigned char NAME(const TYPE *src, int len, TYPE threshold) \
{ \
    unsigned char result = 0; \
    for (int i = 0; i < len; i++) \
    { \
        if (src[i] >= threshold) \
            result += 1 << (7 - i); \
    } \
    return result; \
}

THRESHOLD_INCOMPLETE(threshold_incomplete_u8, unsigned char)
THRESHOLD_INCOMPLETE(threshold_incomplete_int, int)

#define THRESHOLD_1D(NAME, SUFFIX, TYPE) \
static void NAME(unsigned char *dst, \
                 const TYPE *src, \
                 int width, TYPE threshold) \
{ \
    int w8 = width >> 3; \
    for (int i = 0; i < w8; i++) \
        dst[i] = threshold_8_##SUFFIX(src + (i << 3), threshold); \
    if (width & 7) \
        dst[w8] = threshold_incomplete_##SUFFIX(src + (width & ~7), width & 7, threshold); \
}
THRESHOLD_1D(threshold_1d_u8, u8, unsigned char)
THRESHOLD_1D(threshold_1d_int, int, int)

void threshold_u8(unsigned char *dst, int dst_stride,
                  const unsigned char *src, int src_stride,
                  int width, int height, int threshold)
{
    for (int i = 0; i < height; i++)
    {
        threshold_1d_u8(dst + i * dst_stride,
                        src + i * src_stride,
                        width, threshold);
    }
}

void threshold_int(unsigned char *dst, int dst_stride,
                   const int *src, int src_stride,
                   int width, int height, int threshold)
{
    for (int i = 0; i < height; i++)
    {
        threshold_1d_int(dst + i * dst_stride,
                         src + i * src_stride,
                         width, threshold);
    }
}
