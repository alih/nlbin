/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "rank-flt.h"
#include "seesaw.h"
#include "pgm.h"

// RankFilter histogram {{{

typedef struct 
{
    uint32_t counter0;
    uint32_t counters1[4];
    uint32_t counters2[16];
    uint32_t counters3[64];
} RankFilter;
typedef RankFilter RF;

#define ONES 0x1010101u

static void rf_add(RF *rf, unsigned k)
{
    rf->counter0          += ONES << ((k >> 3) & 0x18);
    rf->counters1[k >> 6] += ONES << ((k >> 1) & 0x18);
    rf->counters2[k >> 4] += ONES << ((k << 1) & 0x18);
    rf->counters3[k >> 2] += ONES << ((k << 3) & 0x18);
}

static void rf_adjust(RF *rf, unsigned k, int coef)
{
    int32_t x = coef + (coef << 8);
    x += x << 16;
    rf->counter0          += x << ((k >> 3) & 0x18);
    rf->counters1[k >> 6] += x << ((k >> 1) & 0x18);
    rf->counters2[k >> 4] += x << ((k << 1) & 0x18);
    rf->counters3[k >> 2] += x << ((k << 3) & 0x18);
}

static void rf_subtract(RF *rf, unsigned k)
{
    rf->counter0          -= ONES << ((k >> 3) & 0x18);
    rf->counters1[k >> 6] -= ONES << ((k >> 1) & 0x18);
    rf->counters2[k >> 4] -= ONES << ((k << 1) & 0x18);
    rf->counters3[k >> 2] -= ONES << ((k << 3) & 0x18);
}

static int rf_count(RF *rf)
{
    return rf->counter0 >> 24;
}

// ____________________________________________________

#define SIGNS_MASK 0x80808080

static int seek32_x8(uint32_t acc, unsigned char k)
{
    uint32_t x = k + (k << 8);
    x += x << 16;
    return __builtin_clz((x - acc) & SIGNS_MASK);
}

static int seek32(uint32_t acc, unsigned char k)
{
    return seek32_x8(acc, k) >> 3;
}

static int seek32_alt(uint32_t acc, unsigned char k)
{
    uint32_t x = k + (k << 8);
    x += x << 16;
    return __builtin_ctz((x - acc) & SIGNS_MASK) >> 3;
}

static void seek32_smoke_test()
{
    assert(seek32_x8(0x02040608, 1) == 0);
    assert(seek32_x8(0x02040608, 2) == 0);
    assert(seek32_x8(0x02040608, 3) == 8);
    assert(seek32_x8(0x02040608, 4) == 8);
    assert(seek32_x8(0x02040608, 5) == 16);
    assert(seek32_x8(0x02040608, 6) == 16);
    assert(seek32_x8(0x02040608, 7) == 24);

    assert(seek32_alt(0x08060402, 1) == 0);
    assert(seek32_alt(0x08060402, 2) == 1);
    assert(seek32_alt(0x08060402, 3) == 1);
    assert(seek32_alt(0x08060402, 4) == 2);
    assert(seek32_alt(0x08060402, 5) == 2);
    assert(seek32_alt(0x08060402, 6) == 3);
    assert(seek32_alt(0x08060402, 7) == 3);
}

static unsigned update_rank(uint32_t counter, unsigned rank, int x8)
{
    // printf("update counter: %x, rank: %x, x8: %d\n", counter, rank, x8);
    // printf("counter shifted: %x\n", counter >> (32 - x8));
    return rank - ((counter >> (32 - x8)) & 0xFF);
}

static unsigned update_rank_alt(uint32_t counter, unsigned rank, int pos)
{
    return rank - (((counter << 8) >> (pos << 3)) & 0xFF);
}

static unsigned seek_and_update_rank(uint32_t counter, unsigned rank)
{
    return update_rank(counter, rank, seek32_x8(counter, rank));
}

static unsigned seek_and_update_rank_alt(uint32_t counter, unsigned rank)
{
    return update_rank_alt(counter, rank, seek32_alt(counter, rank));
}

static void update_rank_smoke_test()
{
    assert(seek_and_update_rank_alt(0x08060402, 0) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 1) == 1);
    assert(seek_and_update_rank_alt(0x08060402, 2) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 3) == 1);
    assert(seek_and_update_rank_alt(0x08060402, 4) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 5) == 1);
    assert(seek_and_update_rank_alt(0x08060402, 6) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 7) == 1);
}

static int rf_seek(RF *rf, unsigned char rank)
{
    int c0 = rf->counter0;
    int i0 = seek32_alt(c0, rank);
    rank = update_rank_alt(c0, rank, i0);

    int c1 = rf->counters1[i0];
    int i1 = seek32_alt(c1, rank);
    rank = update_rank_alt(c1, rank, i1);

    int i = (i0 << 2) + i1;
    int c2 = rf->counters2[i];
    int i2 = seek32_alt(c2, rank);
    rank = update_rank_alt(c2, rank, i2);

    i = (i << 2) + i2;
    int c3 = rf->counters3[i];
    int i3 = seek32_alt(c3, rank);
    return (i << 2) + i3;
}

static int rf2_seek(RF *rf1, RF *rf2, unsigned char rank)
{
    int c0 = rf1->counter0 + rf2->counter0;
    int i0 = seek32_alt(c0, rank);
    rank = update_rank_alt(c0, rank, i0);

    int c1 = rf1->counters1[i0] + rf2->counters1[i0];
    int i1 = seek32_alt(c1, rank);
    rank = update_rank_alt(c1, rank, i1);

    int i = (i0 << 2) + i1;
    int c2 = rf1->counters2[i] + rf2->counters2[i];
    int i2 = seek32_alt(c2, rank);
    rank = update_rank_alt(c2, rank, i2);

    i = (i << 2) + i2;
    int c3 = rf1->counters3[i] + rf2->counters3[i];
    int i3 = seek32_alt(c3, rank);
    return (i << 2) + i3;
}

static void rf_clear(RankFilter *rf)
{
    /*
    rf->counter0 = 0;
    memset(rf->counters1, 0, sizeof(rf->counters1));
    memset(rf->counters2, 0, sizeof(rf->counters2));
    memset(rf->counters3, 0, sizeof(rf->counters3));
    */
    memset(rf, 0, sizeof(RF));
}

RankFilter *rf_new(/*int window_size*/)
{
    /*if (window_size >= 128)
    {
        fprintf(stderr, "Error: this implementation supports only windows up to 127\n");
        return NULL;
    }*/
    RF *rf = (RF *) malloc(sizeof(RF));
    rf_clear(rf);
    return rf;
}

void rf_free(RankFilter *rf)
{
    free(rf);
}

void rf_update(RankFilter *rf,
               unsigned char *in,
               int in_len,
               unsigned char *out,
               int out_len)
{
    for (int i = 0; i < in_len; i++)
        rf_add(rf, in[i]);
    for (int i = 0; i < out_len; i++)
        rf_subtract(rf, out[i]);
}

void rf_load(RankFilter *rf,
             const unsigned char *content,
             int len)
{
    rf_clear(rf);
    for (int i = 0; i < len; i++)
        rf_add(rf, content[i]);
}

// RankFilter }}}

/*
int main()
{
    seek32_smoke_test();
    update_rank_smoke_test();

    RF *rf = rf_new(2, 4);
    rf_add(rf, 10);
    rf_add(rf, 20);
    rf_add(rf, 30);
    rf_add(rf, 40);
    printf("rf_seek(2)=%d\n", rf_seek(rf, 2));
    rf_free(rf);

    return 0;
}*/
static int min(int a, int b) { return a < b ? a : b; }

static void
rf_1_init(RankFilter *rf,
          const unsigned char *src, 
          SeesawSegment *seg,
          int n_segs)
{
    rf_clear(rf);
    for (int s = 0; s < n_segs; s++)
    {
        int coef = seg[s].coef;
        int start = seg[s].start;
        int end = seg[s].end;
        for (int i = start; i < end; i++)
        {
            rf_adjust(rf, src[i], coef);
        }
    }
}

static void
rf_2_init(RankFilter *rf,
          const unsigned char *src1, 
          const unsigned char *src2,
          SeesawSegment *seg,
          int n_segs)
{
    rf_clear(rf);
    for (int s = 0; s < n_segs; s++)
    {
        int coef = seg[s].coef;
        int start = seg[s].start;
        int end = seg[s].end;
        for (int i = start; i < end; i++)
        {
            rf_adjust(rf, src1[i], coef);
            rf_adjust(rf, src2[i], coef);
        }
    }
}

static void
rf_init_batch(RankFilter *rfs,
                int n, // strip height == batch_size
                const unsigned char *src, 
                int src_stride,
                SeesawSegment *seg,
                int n_segs)
{
    memset(rfs, 0, sizeof(RankFilter) * n);
    for (int s = 0; s < n_segs; s++)
    {
        int coef = seg[s].coef;
        int start = seg[s].start;
        int end = seg[s].end;
        for (int i = start; i < end; i++)
        {
            for (int y = 0; y < n; y++)
                rf_adjust(&rfs[y], src[y * src_stride + i], coef);
        }
    }
}
            

/**
 * Apply rank filter to 2 lines, store the result in a strided destination. rf_2_init() should be called beforehand.
 */
static void
rf_2_t_line(unsigned char *dst,
            int dst_stride,
            RankFilter *rf,
            int rank,
            const unsigned char *src1, 
            const unsigned char *src2,
            // horiz. scheduled updates as output by seesaw_schedule_updates():
            SeesawUpdate *upd,
            int n_upds)
{
    int i = 0;
    for (int u = 0; u < n_upds; u++)
    {
        int cap = upd[u].cap;
        int left = upd[u].left;
        int right = upd[u].right;

        #define UPD \
            dst[i * dst_stride] = rf_seek(rf, rank); \
            i++; \
            rf_add(rf, src1[right]); \
            rf_add(rf, src2[right]); \
            rf_subtract(rf, src1[left]); \
            rf_subtract(rf, src2[left]);
        switch ((upd[u].left_backwards << 1) + upd[u].right_backwards)
        {
            case 0: while (i < cap) { UPD; left++; right++; } break;
            case 1: while (i < cap) { UPD; left++; right--; } break;
            case 2: while (i < cap) { UPD; left--; right++; } break;
            case 3: while (i < cap) { UPD; left--; right--; } break;
        }
        #undef UPD
    }
}

static void
rf_2_t_batch(unsigned char *dst,
             int dst_stride,
             RankFilter *prev_rf,
             RankFilter *rfs,
             int n, // batch_size
             int rank,
             const unsigned char *prev_line,
             const unsigned char *src, 
             int src_stride,
             // horiz. scheduled updates as output by seesaw_schedule_updates():
             SeesawUpdate *upd,
             int n_upds)
{
    int i = 0;
    for (int u = 0; u < n_upds; u++)
    {
        int cap = upd[u].cap;
        int left = upd[u].left;
        int right = upd[u].right;

        #define UPD \
            dst[i * dst_stride] = rf2_seek(prev_rf, rfs, rank); \
            for (int j = 1; j < n; j++) \
                dst[i * dst_stride + j] = rf2_seek(&rfs[j - 1], &rfs[j], rank); \
            i++; \
            rf_add(prev_rf, prev_line[right]); \
            rf_subtract(prev_rf, prev_line[left]); \
            for (int j = 0; j < n; j++) \
            { \
                rf_add(&rfs[j], src[j * src_stride + right]); \
                rf_subtract(&rfs[j], src[j * src_stride + left]); \
            }
        switch ((upd[u].left_backwards << 1) + upd[u].right_backwards)
        {
            case 0: while (i < cap) { UPD; left++; right++; } break;
            case 1: while (i < cap) { UPD; left++; right--; } break;
            case 2: while (i < cap) { UPD; left--; right++; } break;
            case 3: while (i < cap) { UPD; left--; right--; } break;
        }
        #undef UPD
    }
}

static void
rf_line(unsigned char *dst,
        RankFilter *rf,
        int rank,
        const unsigned char *src, 
        // horiz. scheduled updates as output by seesaw_schedule_updates():
        SeesawUpdate *upd,
        int n_upds)
{
    int i = 0;
    for (int u = 0; u < n_upds; u++)
    {
        int cap = upd[u].cap;
        int left = upd[u].left;
        int right = upd[u].right;

        #define UPD \
            dst[i++] = rf_seek(rf, rank); \
            rf_add(rf, src[right]); \
            rf_subtract(rf, src[left]); 
        switch ((upd[u].left_backwards << 1) + upd[u].right_backwards)
        {
            case 0: while (i < cap) { UPD; left++; right++; } break;
            case 1: while (i < cap) { UPD; left++; right--; } break;
            case 2: while (i < cap) { UPD; left--; right++; } break;
            case 3: while (i < cap) { UPD; left--; right--; } break;
        }
        #undef UPD
    }
}

static void 
rf_2_t(unsigned char *dst,
       int dst_stride,
       const unsigned char *src,
       int width,
       int height,
       int src_stride,
       int window_size,
       int rank,
       int batch_size)
{
    // initial window position
    int initial_right = window_size / 2;
    int initial_left = initial_right - window_size;

    SeesawSegment seg[SEESAW_MAX_SEGMENTS];
    int n_segs = seesaw_segment(seg, width, initial_left, initial_right);

    SeesawUpdate upd[SEESAW_MAX_UPDATES];
    int n_upd = seesaw_schedule_updates(upd,
                   width, initial_left, initial_right); 

    RankFilter *rf = rf_new();
    // XXX spec for line0?
    for (int y = 0; y < height; y++)
    {
        const unsigned char *line1 = y ? src + (y - 1) * src_stride
                                 : src;
        const unsigned char *line2 = src + y * src_stride;
        unsigned char *result_line = dst + y; /* transposed */

        rf_2_init(rf, line1, line2, seg, n_segs);
        rf_2_t_line(result_line, dst_stride, rf, rank, line1, line2,
                    upd, n_upd);
    }
    rf_free(rf);
}

static void 
rf_2_t_batches
      (unsigned char *dst,
       int dst_stride,
       const unsigned char *src,
       int width,
       int height,
       int src_stride,
       int window_size,
       int rank,
       int batch_size)
{
    // initial window position
    int initial_right = window_size / 2;
    int initial_left = initial_right - window_size;

    SeesawSegment seg[SEESAW_MAX_SEGMENTS];
    int n_segs = seesaw_segment(seg, width, initial_left, initial_right);

    SeesawUpdate upd[SEESAW_MAX_UPDATES];
    int n_upd = seesaw_schedule_updates(upd,
                   width, initial_left, initial_right); 

    RF prev_rf;
    RF rfs[batch_size];
    int y = 0;
    while (y < height)
    {
        if (batch_size > height - y)
            batch_size = height - y;
        
        const unsigned char *prev_line = y ? src + (y - 1) * src_stride
                                           : src;
        const unsigned char *top_line = src + y * src_stride;

        rf_1_init(&prev_rf, prev_line, seg, n_segs);
        rf_init_batch(rfs, batch_size, top_line, src_stride, seg, n_segs);

        rf_2_t_batch(dst + y, dst_stride,
             &prev_rf,
             rfs,
             batch_size, // batch_size
             rank,
             prev_line,
             top_line, 
             src_stride,
             upd,
             n_upd);
        y += batch_size;
    }
}

static void 
rf_2(unsigned char *dst,
     int dst_stride,
     const unsigned char *src,
     int width,
     int height,
     int src_stride,
     int window_size,
     int rank)
{
    // initial window position
    int initial_right = window_size / 2;
    int initial_left = initial_right - window_size;
    RankFilter *rf = rf_new();

    SeesawSegment seg[SEESAW_MAX_SEGMENTS];
    int n_segs = seesaw_segment(seg, width, initial_left, initial_right);

    SeesawUpdate upd[SEESAW_MAX_UPDATES];
    int n_upds = seesaw_schedule_updates(upd,
                    width, initial_left, initial_right); 

    for (int y = 0; y < height; y++)
    {
        const unsigned char *line1 = y ? src + (y - 1) * src_stride
                                       : src;
        const unsigned char *line2 = src + y * src_stride;
        unsigned char *result_line = dst + y * dst_stride;

        rf_2_init(rf, line1, line2, seg, n_segs);
        rf_2_t_line(result_line, 1, rf, rank, line1, line2,
                    upd, n_upds);
    }
    rf_free(rf);
}

// Command-line options
const char *input_path = NULL;
const char *output_path = NULL;
int window_size = -1;
int rank = -1;
int percentile = -200;
int batch_size = 32;
     
static void process()
{
    if (rank < 0)
    {
        if (percentile < -100)
        {
            fprintf(stderr, "Specify --percentile or --rank\n");
            exit(EXIT_FAILURE);
        }

        if (percentile < 0)
            percentile += 100; // scipy style

        if (percentile == 100)
            rank = window_size * 2 - 1;
        else
            rank = window_size * 2 * percentile / 100;
    }


    int width, height;
    unsigned char *image = pgm_load(input_path, &width, &height);
    unsigned char *buf = (unsigned char *) malloc(width * height);
    int stride = width;

    clock_t begin = clock();
    rf_2_t_batches(buf, height, image, width, height, width, window_size, rank, batch_size);
    //rf_2(buf, width, image, width, height, width, window_size, rank);
    rf_2_t_batches(image, width, buf, height, width, height, window_size, rank, batch_size);
    clock_t end = clock();
    double elapsed = (end - begin) * 1.0 / CLOCKS_PER_SEC;
    printf("time elapsed: %lf sec\n", elapsed);

    pgm_save(output_path, image, width, height, width);
    //pgm_save(output_path, buf, width, height, width);
    free(image);
    free(buf);
}

// Command-line interface {{{

static bool accept_int_option(const char *opt, const char *next,
                              const char *form1, const char *form2, 
                              int min, int max, int *value, int *index)
{
    if (strcmp(opt, form1) && strcmp(opt, form2))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s/%s\n", form1, form2);
        exit(EXIT_FAILURE);
    }

    char *endptr;
    long v = strtol(next, &endptr, 0);
    if (*endptr)
    {
        fprintf(stderr,
                "Error parsing number for %s/%s option: %s\n", form1, form2, next);
        exit(EXIT_FAILURE);
    }

    if (v < min || v > max)
    {
        fprintf(stderr,
                "The value for %s/%s option should be between %d and %d, but found %ld\n", 
                form1, form2, min, max, v);
        exit(EXIT_FAILURE);
    }

    *value = (int) v;
    (*index)++;
    return true;
}

static void print_usage(const char *prog_name)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s [options] input.pgm output.pgm\n", prog_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -r <number> or --rank <number>        Set rank for the rank filter\n");
    fprintf(stderr, "    -p <number> or --percentile <number>  Set rank through a percentile\n");
    fprintf(stderr, "    -w <number> or --window <number>      Set the window size\n");
    fprintf(stderr, "    -b <number> or --batch <number>       Set the batch size\n");
}

int main(int argc, const char *const *argv)
{
    bool accept_options = true;
    for (int i = 1; i < argc; i++)
    {
        const char *arg = argv[i];
        const char *next = i == argc - 1 ? NULL 
                                         : argv[i + 1];
        if (accept_options && *arg == '-')
        {
            if (!strcmp(arg, "--"))
            {
                accept_options = false;
            }
            else if (!accept_int_option(arg, next, "-r", "--rank", 0, 125, &rank, &i))
                 if (!accept_int_option(arg, next, "-w", "--window", 0, 63, &window_size, &i))
                 if (!accept_int_option(arg, next, "-b", "--batch", 0, INT_MAX, &batch_size, &i))
                 if (!accept_int_option(arg, next, "-p", "--percentile", -100, 100, &percentile, &i))
                    {
                        fprintf(stderr, "Unknown option: %s\n", arg);
                        print_usage(argv[0]);
                        exit(EXIT_FAILURE);
                    }
        }
        else if (!input_path)
            input_path = arg;
        else if (!output_path)
            output_path = arg;
        else
        {
            print_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    if (!input_path || !output_path)
    {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (window_size < 0)
    {
        fprintf(stderr, "Window size not set, use --window\n");
        exit(EXIT_FAILURE);
    }

    if (batch_size < 0)
    {
        fprintf(stderr, "Batch size not set, use --batch\n");
        exit(EXIT_FAILURE);
    }

    process();

    return EXIT_SUCCESS;
}

// Command-line interface }}}

// vim: set foldmethod=marker:
