/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // this file is a test program

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "seesaw.h"

static void get_counters_dumb(int *counters, int start, int end, int h)
{
    memset(counters, 0, sizeof(*counters) * h);
    for (int i = start; i < end; i++)
    {
        int s = seesaw(i, h);
        assert(0 <= s && s < h);
        counters[s]++;
    }
}

static void get_counters_smart(int *counters, int start, int end, int h)
{
    memset(counters, 0, sizeof(*counters) * h);
    SeesawSegment seg[SEESAW_MAX_SEGMENTS];
    int n = seesaw_segment(seg, h, start, end);
    assert(n <= SEESAW_MAX_SEGMENTS);
    for (int i = 0; i < n; i++)
    {
        for (int j = seg[i].start; j < seg[i].end; j++)
            counters[j] = seg[i].coef;
    }
}


static void test_seesaw_segment()
{
    for (int h = 1; h < 10; h++)
    {
        //printf("%d\n", h);
        int c_d[h];
        int c_s[h];
        for (int start = -100; start < 100; start++) 
        for (int end = -100; end < 100; end++)
        {
            //printf("%d %d\n", start, end);
            get_counters_dumb(c_d, start, end, h);
            get_counters_smart(c_s, start, end, h);
            assert(!memcmp(c_d, c_s, h * sizeof(int)));
        }
    }
}

static void test_one_seesaw_schedule_updates(int n, int left, int right)
{
    SeesawUpdate upd[SEESAW_MAX_UPDATES];
    int c = seesaw_schedule_updates(upd, n, left, right);
    int i = 0;
    for (int u = 0; u < c; u++)
    {
        assert(i < upd[u].cap);
        assert(upd[u].cap <= n);
        int l = upd[u].left;
        int r = upd[u].right;
        int l_speed = upd[u].left_backwards  ? -1 : 1;
        int r_speed = upd[u].right_backwards ? -1 : 1;
        for (; i < upd[u].cap; i++)
        {
            assert(l == seesaw(left + i, n));
            assert(r == seesaw(right + i, n));

            l += l_speed;
            r += r_speed;
        }    
        //printf("until %d left_back:%d right_back:%d\n", caps[u], left_backwards[u], right_backwards[u]);
    }
    assert(i == n);
}

static void test_seesaw_schedule_updates()
{
    for (int n = 1; n < 10; n++)
        for (int left = -20; left < 20; left++)
            for (int right = -20; right < 20; right++)
                test_one_seesaw_schedule_updates(n, left, right);
}

int main()
{
    test_seesaw_segment();
    test_seesaw_schedule_updates();
    return 0;
}
