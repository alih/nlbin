/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include "max.h"

int max_2d_int(const int *src, int stride,
               int width, int height)
{
    if (width <= 0 || height <= 0)
        return INT_MIN;

    int best = *src;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            int c = src[y * stride + x];
            if (c > best)
                best = c;
        }
    }
    return best;
}
