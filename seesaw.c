/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <string.h>

#include "seesaw.h"

int seesaw(int x, int height)
{
    if (x < 0)
        x = - x - 1;
    int q = x / height;
    int m = x % height;
    return q & 1 ? height - 1 - m
                 : m;
}

static int seesaw_speed_bit_nonneg(int x, int height)
{
    return (x / height) & 1;
}

static int seesaw_speed_sign_bit(int x, int height)
{
    if (x < 0)
        return 1 - seesaw_speed_bit_nonneg(- x - 1, height);
    return seesaw_speed_bit_nonneg(x, height);   
}

int seesaw_speed(int x, int height)
{
    return 1 - 2 * seesaw_speed_sign_bit(x, height);
}

/*
static int seesaw_speed_nonneg(int x, int height)
{
    int bit = seesaw_speed_bit_nonneg(x, height);
    return 1 - 2 * bit;
}

static int seesaw_speed(int x, int height)
{
    if (x < 0)
        return -seesaw_speed_nonneg(- x - 1, height);
    return seesaw_speed_nonneg(x, height);   
}
*/

static int seesaw_small_segment(SeesawSegment result[SEESAW_MAX_SEGMENTS],
                                int len, int start, int end)
{
    if (start >= end)
        return 0;

    int period = len * 2;

    assert(end - start < period); // this is what "small" means

    if (start >= period)
    {
        int shift = (start / period) * period;
        start -= shift;
        end -= shift;
    }
    else if (start < -period)
    {
        int shift = (-start / period) * period;
        start += shift;
        end += shift;
    }

    if (start < 0)
    {
        start += period;
        end += period;
    }

    assert(start >= 0);
    assert(start < period);
    assert(end >= start);
    assert(end - start < period); // still holds because we shifted start and end together 
    
    if (start < len)
    {
        if (end <= len)
        {
            //    x
            //   x
            //  x
            result[0].coef = 1;
            result[0].start = start;
            result[0].end = end;
            return 1;   
        }
        if (end <= period)
        {
            //    xx
            //   x  x
            //  x
            int start2 = period - end;
            if (start > start2)
            {
                int t = start; start = start2; start2 = t;
            }
            if (start < start2)
            {
                result[0].coef = 1;
                result[0].start = start;
                result[0].end = start2;
                result[1].coef = 2;
                result[1].start = start2;
                result[1].end = len;
                return 2;
            }
            result[0].coef = 2;
            result[0].start = start;
            result[0].end = len;
            return 1;
        }

        // end > period
        //   xx
        //  x  x
        //      x  x
        //       xx
        result[0].coef = 2;
        result[0].start = 0;
        result[0].end = end - period;
        result[1].coef = 1;
        result[1].start = end - period;
        result[1].end = start;
        result[2].coef = 2;
        result[2].start = start;
        result[2].end = len;
        return 3;
    }
    else
    {
        // start >= len

        if (end <= period)
        {
            //  x
            //   x
            //    x
            result[0].coef = 1;
            result[0].start = period - end;
            result[0].end = period - start;
            return 1;   
        }
        if (end <= period + len)
        {
            //  x  
            //   x  x
            //    xx
            int a = period - start;
            int b = end - period;
            if (a > b)
            {
                int t = a; a = b; b = t;
            }
            if (a < b)
            {
                result[0].coef = 2;
                result[0].start = 0;
                result[0].end = a;
                result[1].coef = 1;
                result[1].start = a;
                result[1].end = b;
                return 2;
            }
            result[0].coef = 2;
            result[0].start = 0;
            result[0].end = a;
            return 1;
        }
        // end > period + len
        //       xx
        //      x  x
        //  x  x
        //   xx
        result[0].coef = 2;
        result[0].start = 0;
        result[0].end = period - start;
        result[1].coef = 1;
        result[1].start = period - start;
        result[1].end = 2 * period - end;
        result[2].coef = 2;
        result[2].start = 2 * period - end;
        result[2].end = len;
        return 3;
    }
}

int seesaw_segment(SeesawSegment result[SEESAW_MAX_SEGMENTS],
                   int len, int start, int end)
{
    assert(len >= 1);
    int period = len * 2;
    
    if (end - start < period)
        return seesaw_small_segment(result, len, start, end);

    int total = (end - start) / period; // amount of periods completely covered
    end -= total * period;
    total *= 2;

    int c = seesaw_small_segment(result, len, start, end);
    if (!c)
    {
        result[0].coef = total;
        result[0].start = 0;
        result[0].end = len;
        return 1;
    }

    if (result[0].start)
    {
        assert(c < SEESAW_MAX_SEGMENTS);
        for (int i = c; i > 0; i--)
            result[i] = result[i - 1];
        result[0].coef = 0;
        result[0].start = 0;
        result[0].end = result[1].start;
        c++;
    }
    if (result[c - 1].end < len)
    {
        assert(c < SEESAW_MAX_SEGMENTS);
        result[c].coef = 0;
        result[c].start = result[c - 1].end;
        result[c].end = len;
        c++;
    }
    for (int i = 0; i < c; i++)
        result[i].coef += total;

    return c;
}

int seesaw_schedule_updates(
    SeesawUpdate result[SEESAW_MAX_UPDATES],
    int n,
    int left,
    int right)
{
    int l = seesaw(left, n);
    int r = seesaw(right, n);
    int lb = seesaw_speed_sign_bit(left, n);
    int rb = seesaw_speed_sign_bit(right, n);

    int update_counter = 0;
    for (int i = 0; i < n;)
    {
        result[update_counter].left = l;
        result[update_counter].right = r;
        result[update_counter].left_backwards = lb;
        result[update_counter].right_backwards = rb;

        int l_event = lb ? l + 1 : n - l;
        int r_event = rb ? r + 1 : n - r;
        int i_event = n - i;

        if (i_event <= l_event && i_event <= r_event)
        {
            result[update_counter].cap = n;
            return update_counter + 1;
        } 

        if (l_event <= r_event)
        {
            i += l_event;
            l = lb ? 0 : n - 1;
            lb = 1 - lb;
            if (rb)
            {
                r -= l_event;
                if (r < 0)
                {
                    r = 0;
                    rb = 0;
                }
            }
            else
            {
                r += l_event;
                if (r == n)
                {
                    r = n - 1;
                    rb = 1;
                }
            }
        }
        else
        {
            i += r_event;
            r = rb ? 0 : n - 1;
            rb = 1 - rb;
            if (lb)
            {
                l -= r_event;
                if (l < 0)
                {
                    l = 0;
                    lb = 0;
                }
            }
            else
            {
                l += r_event;
                if (l == n)
                {
                    l = n - 1;
                    lb = 1;
                }
            }
        }

        result[update_counter++].cap = i;
        assert(update_counter < SEESAW_MAX_UPDATES);
        assert(l == seesaw(left + i, n));
        assert(r == seesaw(right + i, n));
        assert(lb == seesaw_speed_sign_bit(left + i, n));
        assert(rb == seesaw_speed_sign_bit(right + i, n));
    }
    return update_counter;
}
