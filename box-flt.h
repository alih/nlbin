/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NLBIN_BOX_FLT_H
#define NLBIN_BOX_FLT_H

/**
 * Compute the sum on a [left:right] segment.
 * [left:right] may be arbitrarily larger than [0:len];
 * the boundaries are handled using reflection.
 */  
int box_sum_u8_once(const unsigned char *src, int len, 
                    int left, int right);

void box_sum_u8(int *dst, 
                const unsigned char *src, int len, 
                int initial_left, 
                int initial_right);

int box_sum_int_once(const int *src, int len, 
                    int left, int right);

void box_sum_int(int *dst, 
                 const int *src, int len, 
                 int initial_left, 
                 int initial_right);

#endif
