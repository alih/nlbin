/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rank-flt.h"

struct RankFilter
{
    int rank;
    uint32_t counter0;
    uint32_t counters1[4];
    uint32_t counters2[16];
    uint32_t counters3[64];
};
typedef RankFilter RF;

#define ONES 0x1010101u

static void rf_add(RF *rf, unsigned k)
{
    rf->counter0          += ONES << ((k >> 3) & 0x18);
    rf->counters1[k >> 6] += ONES << ((k >> 1) & 0x18);
    rf->counters2[k >> 4] += ONES << ((k << 1) & 0x18);
    rf->counters3[k >> 2] += ONES << ((k << 3) & 0x18);
}

static void rf_adjust(RF *rf, unsigned k, int coef)
{
    int32_t x = coef + (coef << 8);
    x += x << 16;
    rf->counter0          += x << ((k >> 3) & 0x18);
    rf->counters1[k >> 6] += x << ((k >> 1) & 0x18);
    rf->counters2[k >> 4] += x << ((k << 1) & 0x18);
    rf->counters3[k >> 2] += x << ((k << 3) & 0x18);
}

static void rf_subtract(RF *rf, unsigned k)
{
    rf->counter0          -= ONES << ((k >> 3) & 0x18);
    rf->counters1[k >> 6] -= ONES << ((k >> 1) & 0x18);
    rf->counters2[k >> 4] -= ONES << ((k << 1) & 0x18);
    rf->counters3[k >> 2] -= ONES << ((k << 3) & 0x18);
}

static int rf_count(RF *rf)
{
    return rf->counter0 >> 24;
}

// ____________________________________________________

#define SIGNS_MASK 0x80808080

static int seek32_x8(uint32_t acc, unsigned char k)
{
    uint32_t x = k + (k << 8);
    x += x << 16;
    return __builtin_clz((x - acc) & SIGNS_MASK);
}

static int seek32(uint32_t acc, unsigned char k)
{
    return seek32_x8(acc, k) >> 3;
}

static int seek32_alt(uint32_t acc, unsigned char k)
{
    uint32_t x = k + (k << 8);
    x += x << 16;
    return __builtin_ctz((x - acc) & SIGNS_MASK) >> 3;
}

static void seek32_smoke_test()
{
    assert(seek32_x8(0x02040608, 1) == 0);
    assert(seek32_x8(0x02040608, 2) == 0);
    assert(seek32_x8(0x02040608, 3) == 8);
    assert(seek32_x8(0x02040608, 4) == 8);
    assert(seek32_x8(0x02040608, 5) == 16);
    assert(seek32_x8(0x02040608, 6) == 16);
    assert(seek32_x8(0x02040608, 7) == 24);

    assert(seek32_alt(0x08060402, 1) == 0);
    assert(seek32_alt(0x08060402, 2) == 1);
    assert(seek32_alt(0x08060402, 3) == 1);
    assert(seek32_alt(0x08060402, 4) == 2);
    assert(seek32_alt(0x08060402, 5) == 2);
    assert(seek32_alt(0x08060402, 6) == 3);
    assert(seek32_alt(0x08060402, 7) == 3);
}

static unsigned update_rank(uint32_t counter, unsigned rank, int x8)
{
    printf("update counter: %x, rank: %x, x8: %d\n", counter, rank, x8);
    printf("counter shifted: %x\n", counter >> (32 - x8));
    return rank - ((counter >> (32 - x8)) & 0xFF);
}

static unsigned update_rank_alt(uint32_t counter, unsigned rank, int pos)
{
    return rank - (((counter << 8) >> (pos << 3)) & 0xFF);
}

static unsigned seek_and_update_rank(uint32_t counter, unsigned rank)
{
    return update_rank(counter, rank, seek32_x8(counter, rank));
}

static unsigned seek_and_update_rank_alt(uint32_t counter, unsigned rank)
{
    return update_rank_alt(counter, rank, seek32_alt(counter, rank));
}

static void update_rank_smoke_test()
{
    assert(seek_and_update_rank_alt(0x08060402, 0) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 1) == 1);
    assert(seek_and_update_rank_alt(0x08060402, 2) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 3) == 1);
    assert(seek_and_update_rank_alt(0x08060402, 4) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 5) == 1);
    assert(seek_and_update_rank_alt(0x08060402, 6) == 0);
    assert(seek_and_update_rank_alt(0x08060402, 7) == 1);
}

static int rf_seek(RF *rf, unsigned char rank)
{
    int c0 = rf->counter0;
    int i0 = seek32_alt(c0, rank);
    rank = update_rank_alt(c0, rank, i0);

    int c1 = rf->counters1[i0];
    int i1 = seek32_alt(c1, rank);
    rank = update_rank_alt(c1, rank, i1);

    int i = (i0 << 2) + i1;
    int c2 = rf->counters2[i];
    int i2 = seek32_alt(c2, rank);
    rank = update_rank_alt(c2, rank, i2);

    i = (i << 2) + i2;
    int c3 = rf->counters3[i];
    int i3 = seek32_alt(c3, rank);
    return (i << 2) + i3;
}

void rf_clear(RankFilter *rf)
{
    rf->counter0 = 0;
    memset(rf->counters1, 0, sizeof(rf->counters1));
    memset(rf->counters2, 0, sizeof(rf->counters2));
    memset(rf->counters3, 0, sizeof(rf->counters3));
}

RankFilter *rf_new(int rank, int window_size)
{
    if (window_size >= 128)
    {
        fprintf(stderr, "Error: this implementation supports only windows up to 127\n");
        return NULL;
    }
    RF *rf = (RF *) malloc(sizeof(RF));
    rf->rank = rank;
    rf_clear(rf);
    return rf;
}
void rf_free(RankFilter *rf)
{
    free(rf);
}

void rf_update(RankFilter *rf,
               unsigned char *in,
               int in_len,
               unsigned char *out,
               int out_len)
{
    for (int i = 0; i < in_len; i++)
        rf_add(rf, in[i]);
    for (int i = 0; i < out_len; i++)
        rf_subtract(rf, out[i]);
}

void rf_load(RankFilter *rf,
             const unsigned char *content,
             int len)
{
    rf_clear(rf);
    for (int i = 0; i < len; i++)
        rf_add(rf, content[i]);
}

void rf_check(RankFilter *rf,
              const unsigned char *content,
              int len)
{
    // XXX
}

unsigned char rf_get(RankFilter *rf)
{
    return rf_seek(rf, rf->rank);
}

/*
int main()
{
    seek32_smoke_test();
    update_rank_smoke_test();

    RF *rf = rf_new(2, 4);
    rf_add(rf, 10);
    rf_add(rf, 20);
    rf_add(rf, 30);
    rf_add(rf, 40);
    printf("rf_seek(2)=%d\n", rf_seek(rf, 2));
    rf_free(rf);

    return 0;
}*/
