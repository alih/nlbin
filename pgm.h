/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NLBIN_PGM_H
#define NLBIN_PGM_H

#include <stdbool.h>
#include <stdio.h>

bool pgm_read_header(FILE *f, int *out_width, int *out_height);
unsigned char *pgm_read(FILE *f, int *out_width, int *out_height);
unsigned char *pgm_load(const char *path, int *out_width, int *out_height);
void pgm_save(const char *path, unsigned char *image, int width, int height, int stride);

#endif
