/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // this file is a test program

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "box-flt.h"
#include "seesaw.h"

static void dumb_box_sum_u8(int *dst, 
                            const unsigned char *src, int len, 
                            int initial_left, 
                            int initial_right)
{
    for (int i = 0; i < len; i++)
    {
        int sum = 0;
        for (int j = initial_left + i; j < initial_right + i; j++)
        {
            sum += src[seesaw(j, len)];
        }
        dst[i] = sum;
    }
}

static void dumb_box_sum_int(int *dst, 
                             const int *src, int len, 
                             int initial_left, 
                             int initial_right)
{
    for (int i = 0; i < len; i++)
    {
        int sum = 0;
        for (int j = initial_left + i; j < initial_right + i; j++)
        {
            sum += src[seesaw(j, len)];
        }
        dst[i] = sum;
    }
}

int main()
{
    for (int n = 0; n < 10; n++)
    {
        for (int iter = 0; iter < 10; iter++)
        {
            unsigned char src_u8[n];
            for (int i = 0; i < n; i++)
                src_u8[i] = (unsigned char) rand();

            int src_int[n];
            for (int i = 0; i < n; i++)
                src_int[i] = rand();
            
            for (int initial_left = -20; initial_left < 20; initial_left++)
            {
                for (int initial_right = initial_left; initial_right < 20; initial_right++)
                {
                    int dst_u8[n];
                    int dst_u8_dumb[n];
                    int dst_int[n];
                    int dst_int_dumb[n];
                    box_sum_u8(dst_u8, src_u8, n, initial_left, initial_right);
                    dumb_box_sum_u8(dst_u8_dumb, src_u8, n, initial_left, initial_right);

                    box_sum_int(dst_int, src_int, n, initial_left, initial_right);
                    dumb_box_sum_int(dst_int_dumb, src_int, n, initial_left, initial_right);

                    assert(!memcmp(dst_u8,  dst_u8_dumb,  n * sizeof(int)));
                    assert(!memcmp(dst_int, dst_int_dumb, n * sizeof(int)));
                }
            }
        }
    }
    return 0;
}
