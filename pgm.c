/*
 * nlbin - a reimplementation of nlbin algorithm in C
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pgm.h"

static void skip_comment_line(FILE *file)
{
    while (1) switch(fgetc(file))
    {
        case EOF: case '\r': case '\n':
            return;
    }
}


static void skip_whitespace_and_comments(FILE *file)
{
    int c = fgetc(file);
    while(1) switch(c)
    {
        case '#':
            skip_comment_line(file);
            /* fall through */
        case ' ': case '\t': case '\r': case '\n':
            c = fgetc(file);
        break;
        case EOF:
            return;
        default:
            ungetc(c, file);
            return;
    }
}

bool pgm_read_header(FILE *f, int *out_width, int *out_height)
{
    if (getc(f) != 'P')
    {
        fprintf(stderr, "Error: PGM file doesn't start with 'P'\n");
        return false;
    }
    
    char type = getc(f);
    if (type != '5')
    {
        fprintf(stderr, "Error: Only (raw) PGM supported\n");
        return false;
    }
    skip_whitespace_and_comments(f);
    
    int max_value;
    if (fscanf(f, "%d %d %d", out_width, out_height, &max_value) != 3)
    {
        fprintf(stderr, "Error: Unable to read width, height and max value\n");
        return false;
    }

    if (*out_width <= 0 || *out_height <= 0)
    {
        fprintf(stderr, "Error: PGM size must be greater than 0\n");
        return false;
    }

    if (max_value != 255)
    {
        fprintf(stderr, "Error: Only max value of 255 is supported\n");
        return false;
    }

    switch(fgetc(f))
    {
        case ' ': case '\t': case '\r': case '\n':
            break;
        default:
            fprintf(stderr, "corrupted PGM, current offset is %ld\n", ftell(f));
            return false;
    }

    return true;
}

unsigned char *pgm_read(FILE *f, int *out_width, int *out_height)
{
    if (!pgm_read_header(f, out_width, out_height))
        return NULL;

    unsigned size = *out_width * *out_height;

    unsigned char *result = (unsigned char *) malloc(size);
    if (fread(result, 1, size, f) != size)
    {
        fprintf(stderr, "Error: Unable to read %d bytes from a PGM file\n", size);
        exit(EXIT_FAILURE);
    }
    return result;
}

unsigned char *pgm_load(const char *path, int *out_width, int *out_height)
{
    FILE *f = fopen(path, "rb");
    if (!f)
    {
        perror(path);
        exit(EXIT_FAILURE);
    }
    unsigned char *result = pgm_read(f, out_width, out_height);
    fclose(f);
    return result;
}

void pgm_save(const char *path, unsigned char *image, int width, int height, int stride)
{
    FILE *f = fopen(path, "wb");
    if (!f)
    {
        perror(path);
        exit(EXIT_FAILURE);
    }

    fprintf(f, "P5\n%d %d 255\n", width, height);

    if (stride == width)
    {
        fwrite(image, width, height, f);
    }
    else
    {
        for (int i = 0; i < height; i++)
            fwrite(image + i * stride, width, 1, f);
    }

    fclose(f);
}
